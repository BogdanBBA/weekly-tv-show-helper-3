unit functii;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ShlObj, data_types, URLMon, WinINet;

type ByteSet=set of byte;

function FormatLastEpisode(x: word): string;
function FormatLastEpisode2(x: word): string;
function FuncAvail(_dllname, _funcname: string; var _p: pointer): boolean;
function GetInetFile(const fileURL, FileName: String): boolean;
function min(x, y: longint): longint;
function max(x, y: longint): longint;
function setofbyte_count(x: ByteSet): word;
function downcase(c: char): char; Overload;
function downcase(c: string): string; Overload;
function fileword(s: string): string;
procedure clean_str(var s: string; Edges, NoMoreThanOneSpaceAtATime: boolean);
function inttoboolean(x: word): boolean;
function booleantotruefalse(x: boolean): string;
function truefalsetoboolean(x: string; signalOnError: boolean): boolean;
function strtocase(Selector : string; CaseList: array of string): Integer;
function upcase(s: string): string; Overload;
function tkb(y: longint): string;
function gender(sex, word_id: byte; capitalize: boolean): string;
function CalculateAge(Birthday, CurrentDate: string): Integer;
function number_suffix(x: longint): string;
function noext(s: string): string;
function GetDirSize(dir: string; subdir: Boolean): Longint;
function song_length(x: Cardinal): string;
function tdhms2b(value: int64): string;
function FScor(r, rc: integer; vr: word): word;
function FYear(x: integer): string;
function FontToStr(Font: TFont): string;
function StrToFont(const s: string; var Font: TFont): boolean;
function nextd(x: word): word;
function prevd(x: word): word;
function GetDesktopFolder: string;
function download(web_addr, dest_name: string): boolean;
function web_str(s: string): string;

implementation

function FormatLastEpisode(x: word): string;
begin result:=Format(sett.lastEpisodeFormatting, [x div 1000, x mod 1000]) end;

function FormatLastEpisode2(x: word): string;
begin result:=Format(sett.lastEpisodeFormatting2, [x div 1000, x mod 1000]) end;

function FuncAvail(_dllname, _funcname: string; var _p: pointer): boolean; //return True if _funcname exists in _dllname
{use:
var InetIsOffline : function(dwFlags: DWORD): BOOL; stdcall;
begin
  if FuncAvail('URL.DLL', 'InetIsOffline', @InetIsOffline) then
    if InetIsOffLine(0) = true then ShowMessage('Not connected')
    else ShowMessage('Connected!')
end;}
var _lib: tHandle;
begin
  Result := false;
  if LoadLibrary(PChar(_dllname)) = 0 then exit;
  _lib := GetModuleHandle(PChar(_dllname)) ;
  if _lib <> 0 then
    begin _p := GetProcAddress(_lib, PChar(_funcname)); if _p <> NIL then Result := true end
end;

function GetInetFile (const fileURL, FileName: String): boolean;
const BufferSize = 1024;
var hSession, hURL: HInternet; Buffer: array[1..BufferSize] of Byte; BufferLen: DWORD; f: File; sAppName: string;
begin
  result := false;
  sAppName := ExtractFileName(Application.ExeName) ;
  hSession := InternetOpen(PChar(sAppName), INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0) ;
  try
    hURL := InternetOpenURL(hSession, PChar(fileURL), nil, 0, 0, 0) ;
    try
      AssignFile(f, FileName) ;
      Rewrite(f,1) ;
      repeat
        InternetReadFile(hURL, @Buffer, SizeOf(Buffer), BufferLen) ;
        BlockWrite(f, Buffer, BufferLen)
      until BufferLen = 0;
      CloseFile(f) ;
      result := True;
    finally
      InternetCloseHandle(hURL)
    end
  finally
    InternetCloseHandle(hSession)
  end
end;

function min(x, y: longint): longint;
begin if x>y then min:=y else min:=x end;

function max(x, y: longint): longint;
begin if x<y then max:=y else max:=x end;

function setofbyte_count(x: ByteSet): word;
var i, n: word;
begin n:=0; for i:=low(byte) to high(byte) do if i in x then inc(n); result:=n end;

function downcase(c: char): char; Overload;
begin if ((c>='A') and (c<='Z')) then downcase:=chr(ord(c)+32) else downcase:=c; end;

function downcase(c: string): string; Overload;
var t: string; i: word;
begin t:=''; for i:=1 to length(c) do if ((c[i]>='A') and (c[i]<='Z')) then t:=t+chr(ord(c[i])+32) else t:=t+c[i]; downcase:=t; end;

function fileword(s: string): string;
var rez: string; i: word;
begin rez:=''; s:=downcase(s); for i:=1 to length(s) do if charinset(s[i], ['0'..'9', 'a'..'z', 'A'..'Z']) then rez:=rez+s[i]; fileword:=rez end;

procedure clean_str(var s: string; Edges, NoMoreThanOneSpaceAtATime: boolean);
var t, x, y: string;
begin
  if s='' then exit; t:=s;
  if Edges=true then
    begin while t[1]=' ' do delete(t, 1, 1); while t[length(t)]=' ' do delete(t, length(t), 1) end;
  if NoMoreThanOneSpaceAtATime then
    begin
      x:=t;
      while length(x)>1 do begin if ((x[1]<>' ') or ((x[1]=' ') and (x[2]<>' '))) then y:=y+x[1]; delete(x, 1, 1) end;
      t:=y+x[1];
    end; s:=t
end;

//

function inttoboolean(x: word): boolean;
begin inttoboolean:=false; if x=0 then inttoboolean:=false else if x=1 then inttoboolean:=true else begin showmessage('ERROR: inttoboolean: invalid integer; x='+inttostr(x)); exit end end;

function booleantotruefalse(x: boolean): string;
begin if x=true then result:='true' else result:='false' end;

function truefalsetoboolean(x: string; signalOnError: boolean): boolean;
begin result:=false; if x='true' then result:=true else if x<>'false' then if signalOnError then showmessage('truefalsetoboolean ERROR: invalid x="'+x+'"') end;

function strtocase(Selector : string; CaseList: array of string): Integer;
var cnt: integer;
begin
  Result:=0; //clean_str(selector, true, false);
  for cnt:=0 to Length(CaseList)-1 do
    begin
      //clean_str(caselist[cnt], true, false);
      if CompareText(Selector, CaseList[cnt]) = 0 then
        begin Result:=cnt+1; Break end
    end
end;

function upcase(s: string): string; Overload;
var i: word;
begin
  for i:=1 to length(s) do s[i]:=upcase(s[i]); upcase:=s;
end;

function tkb(y: longint): string;
var x: real;
begin
  if y<=0 then tkb:='0 B' else
  if ((y>=1) and (y<=999)) then
    begin tkb:=inttostr(y)+' B'; end
  else
    begin
      x:=y;
      if ((x>=1000) and (x<=999999)) then
        begin x:=x/1024; tkb:=formatfloat('0.00', x)+' KB'; end
      else
        begin x:=x/1024/1024; tkb:=formatfloat('0.00', x)+' MB'; end
    end;
end;

function gender(sex, word_id: byte; capitalize: boolean): string;
var r: string;
begin
  r:=''; if not (sex in [1, 2]) then begin showmessage('gender ERROR: sex<>[1, 2]; sex = '+inttostr(sex)); exit end;
  case word_id of
  1: if sex=1 then r:='he' else if sex=2 then r:='she';
  2: if sex=1 then r:='his' else if sex=2 then r:='her';
  3: if sex=1 then r:='man' else if sex=2 then r:='woman';
  else showmessage('gender ERROR: word_id invalid; word_id = '+inttostr(word_id));
  end;
  if (capitalize and (length(r)>0)) then r[1]:=upcase(r[1]);
  gender:=r
end;

function CalculateAge(Birthday, CurrentDate: string): Integer;
var Month, Day, Year, CurrentYear, CurrentMonth, CurrentDay: Word;
begin
  if length(birthday)<>10 then begin showmessage('calculateage ERROR, length('+birthday+') <> 10'); exit end;
  month:=strtoint(copy(birthday, 6, 2)); day:=strtoint(copy(birthday, 9, 2)); year:=strtoint(copy(birthday, 1, 4));
  Currentmonth:=strtoint(copy(CurrentDate, 6, 2)); Currentday:=strtoint(copy(CurrentDate, 9, 2)); Currentyear:=strtoint(copy(CurrentDate, 1, 4));
  if (Year = CurrentYear) and (Month = CurrentMonth) and (Day = CurrentDay) then Result := 0
  else
    begin
      Result := CurrentYear - Year;
      if (Month > CurrentMonth) then Dec(Result)
      else begin if Month = CurrentMonth then if (Day > CurrentDay) then Dec(Result); end;
    end;
  //if (Year = 1900) and (Month = 1) and (Day = 1) then Result := 0
end;

function number_suffix(x: longint): string;
var r: string;
begin
  r:='#ERR(suffix: unknown x mod 20='+inttostr(abs(x) mod 20)+')';
  case abs(x) mod 20 of
  1: r:='st';
  2: r:='nd';
  3: r:='rd';
  0, 4..19: r:='th';
  end;
  number_suffix := r
end;

function noext(s: string): string;
var k: word;
begin
  if pos('.', s)<>0 then begin k:=length(s); while s[k]<>'.' do inc(k, -1); delete(s, k, 10) end; noext:=s
end;

function GetDirSize(dir: string; subdir: Boolean): Longint;
var
  rec: TSearchRec;
  found: Integer;
begin
  Result := 0;
  if dir[Length(dir)] <> '\' then dir := dir + '\';
  found := FindFirst(dir + '*.*', faAnyFile, rec);
  while found = 0 do
  begin
    Inc(Result, rec.Size);
    if (rec.Attr and faDirectory > 0) and (rec.Name[1] <> '.') and (subdir = True) then
      Inc(Result, GetDirSize(dir + rec.Name, True));
    found := FindNext(rec);
  end;
  FindClose(rec);
end;

function song_length(x: Cardinal): string;
begin song_length := inttostr(x div 60)+':'+formatfloat('00', x mod 60) end;

function tdhms2b(value: int64): string;
  function stri(value, typ: word): string;
  var r: string;
  begin
    if value=0 then
      begin stri:=''; exit end
    else
      begin
        case typ of
        0: r:='d';
        1: r:='hr';
        2: r:='min';
        3: r:='sec';
        4: r:='msec';
        end;
        stri:=', '+inttostr(value)+' '+r
      end
  end;
var nday, nhou, nmin: word; nsec, nmsec: longint; r: string; value2: int64;
begin
  value2:=value; value:=value div 1000;
  nday:=value div (3600*24); nhou:=(value mod (3600*24)) div 3600; nmin:=(value mod 3600) div 60; nsec:=value mod 60; nmsec:=value2 mod 1000;
  r:=stri(nday, 0)+stri(nhou, 1)+stri(nmin, 2)+stri(nsec, 3)+stri(nmsec, 4); tdhms2b:=copy(r, 3, length(r)-2)
end;

function FScor(r, rc: integer; vr: word): word;
var sc, d: word;
begin
  sc:=0; d:=0;
  if vr=0 then begin showmessage('function scor ERROR: value range = 0 (r='+inttostr(r)+', rc='+inttostr(rc)+')'); exit end;
  if (r<rc-vr) or (r>rc+vr) then begin log('FScor: scor=0 (see below)'); sc:=0 end
  else
    begin
      d:=abs(rc-r); log('FScor: answer OK: difference='+inttostr(d));
      sc := round( 1000 - 1000*d/vr )
    end;
  log('FScor(r='+inttostr(r)+', rc='+inttostr(rc)+', vr='+inttostr(vr)+': ['+inttostr(rc-vr)+'..'+inttostr(rc+vr)+']), result='+inttostr(sc));
  FScor := sc
end;

function FYear(x: integer): string;
begin
  if x=0 then FYear:='the year 0' else if x<0 then FYear:=inttostr(abs(x))+' BC' else FYear:='AD '+inttostr(x)
end;

function FontToStr(Font: TFont): string;
var sColor, sStyle : string;
begin
  sColor := '$' +IntToHex(ColorToRGB(Font.Color), 8);
  sStyle := IntToStr( byte(Font.Style) );
  result := Font.Name +'|'+ IntToStr(Font.Size) +'|'+sColor +'|'+sStyle;
end;

function StrToFont(const s: string; var Font: TFont): boolean;
var afont : TFont; Strs : TStringList;
begin
  try
    //log('StrToFont S="'+s+'"');
    afont := TFont.Create;
    if Font=nil then Font:=Tfont.Create;
    try
      afont.Assign(Font);
      Strs := TStringList.Create;
      try
        Strs.Text := StringReplace(s, '|', #10, [rfReplaceAll]);
        result := Strs.Count = 4;
        if result then
          begin
            afont.Name := Strs[0];
            afont.Size := StrToInt(Strs[1]);
            afont.Color := StrToInt(Strs[2]);
            afont.Style := TFontStyles(byte(StrToInt(Strs[3])));
          end;
        Font.Assign(afont);
      except on E:Exception do log('StrToFont error (inner):'+dnl+E.ClassName+' - "'+E.Message+'"') end
    except on E:Exception do log('StrToFont error (outer):'+dnl+E.ClassName+' - "'+E.Message+'"') end
  finally
    Strs.Free;
    afont.Free
  end
end;

function nextd(x: word): word;
begin if x=7 then nextd:=1 else nextd:=x+1 end;

function prevd(x: word): word;
begin if x=1 then prevd:=7 else prevd:=x-1 end;

function GetDesktopFolder: string;
var
 buf: array[0..MAX_PATH] of char;
 pidList: PItemIDList;
begin
 Result := 'No Desktop Folder found.';
 SHGetSpecialFolderLocation(Application.Handle, CSIDL_DESKTOP, pidList);
 if (pidList <> nil) then
  if (SHGetPathFromIDList(pidList, buf)) then
    Result := buf;
end;

function download(web_addr, dest_name: string): boolean;
begin
  try Result := UrlDownloadToFile(nil, PChar(web_addr), PChar(dest_name), 0, nil) = 0
  except Result := False end
end;

function web_str(s: string): string;
begin
  result := stringreplace(s, ' ', '+', [rfReplaceAll])
end;

end.

