unit Help2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.OleCtrls, SHDocVw, XML.VerySimple, Vcl.ImgList;

type
  TFH = class(TForm)
    WB: TWebBrowser;
    closeB: TButton;
    refWBB: TButton;
    Panel1: TPanel;
    format1: TLabel;
    format2: TLabel;
    ImageList1: TImageList;
    procedure closeBClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure format2MouseEnter(Sender: TObject);
    procedure format2MouseLeave(Sender: TObject);
    procedure format2Click(Sender: TObject);
    procedure refWBBClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type TLabelSection=record
  lab: TLabel;
  sub: array of TLabel;
end;

var
  FH: TFH;
  ll: array of TLabelSection;
  x: TXMLverySimple;

implementation

{$R *.dfm}

uses data_types, functii, ShellAPI;

procedure TFH.closeBClick(Sender: TObject);
begin
  FH.Close
end;

procedure TFH.FormResize(Sender: TObject);
begin
  Panel1.Left:=0; Panel1.Top:=0; Panel1.Height:=FH.Height;
  WB.Width:=FH.Width-WB.Left-24; WB.Height:=FH.Height-WB.Top-100;
  refWBB.Left:=WB.Left+WB.Width-refWBB.Width+1;
  closeB.Left:=WB.Left+WB.Width-closeB.Width; closeB.Top:=WB.Top+WB.Height+6
end;

procedure TFH.FormShow(Sender: TObject);
begin
  format2Click(ll[0].sub[1])
end;

procedure TFH.refWBBClick(Sender: TObject);
begin WB.Refresh end;

procedure TFH.format2Click(Sender: TObject);
var i, j: word;
begin
  try
  WB.Navigate(GetCurrentDir+'\'+TLabel(Sender).Hint);
  for i:=0 to length(ll)-1 do for j:=0 to length(ll[i].sub)-1 do ll[i].sub[j].Font.Style:=[];
  TLabel(Sender).Font.Style:=[fsBold]
  except Exit end
end;

procedure TFH.format2MouseEnter(Sender: TObject);
begin TLabel(Sender).Font.Color:=clYellow end;

procedure TFH.format2MouseLeave(Sender: TObject);
begin TLabel(Sender).Font.Color:=format2.Font.Color end;

procedure TFH.FormCreate(Sender: TObject);
var i, j, lastBottom: word;
begin
  x:=TXMLVerySimple.Create; x.LoadFromFile('data\help\helpFileStructure.xml');
  setlength(ll, x.Root.ChildNodes.Count); lastBottom:=0;
  for i:=0 to length(ll)-1 do
    begin
      ll[i].lab:=TLabel.Create(Self); ll[i].lab.Parent:=Panel1; ll[i].lab.Font:=format1.Font;
      ll[i].lab.Left:=8; ll[i].lab.Top:=lastBottom+8; lastBottom:=ll[i].lab.Top+ll[i].lab.Height;
      ll[i].lab.Caption:=x.Root.ChildNodes[i].Attribute['caption'];
      setlength(ll[i].sub, x.Root.ChildNodes[i].ChildNodes.Count);
      for j:=0 to length(ll[i].sub)-1 do
        begin
          ll[i].sub[j]:=TLabel.Create(Self);
          with ll[i].sub[j] do
            begin
              Parent:=Panel1; Font:=format2.Font; Left:=16; Top:=lastBottom+8; lastBottom:=Top+Height;
              Cursor:=crHandPoint; OnClick:=format2Click; OnMouseEnter:=format2MouseEnter; OnMouseLeave:=format2MouseLeave;
              Caption:=x.Root.ChildNodes[i].ChildNodes[j].Attribute['label'];
              Hint:='data\help\'+x.Root.ChildNodes[i].ChildNodes[j].Attribute['file']+'.html';
              if not fileexists(Hint) then
                CopyFile('data\help\generic.html', PChar(Hint), true)
            end
        end
    end
end;

end.
