================================================================================
    TEXT EXPORT

    Date: Thursday, 19 April 2012, 11:04
    Number of shows: 45
    Active / inactive: 25 / 19
    Over 7 day(s)
    Shows per day: average 6.43 (range 4-9)
    Broadcasting hour: average 18:47 (range 0-23)
    Ratings: average 7.61 (range 3-10)
    Oldest last watched episode: 2 December 1972 (Episode 312 from Mary Tyler Moore Show)"
    Newest last watched episode: 11 April 2012 (Episode 101 from Don't Trust the B. in Apartment 23)"
    Average date of last watched episode: 18 May 2009
    Photos (total size): 37 (/44, 3.31 MB)
    Photo sizes: average 91.59 KB (range 34.96 KB-267.58 KB)
    Theme songs (total size): 13 (/44, 36.22 MB)
    Theme song sizes: average 2.79 MB (range 139.83 KB-11.32 MB)
    Total utility files size: 39.53 MB (50 files, avg. 809.56 KB)
    Memory used for one show: 152 B
    Memory used for all data: 10.79 KB
================================================================================

========================================
        1. 2 Broke Girls
========================================
    Is being watched at this time
    Broadcasts Mondays at 20:00
    Last episode: 108
    Has a rating of 5/10
========================================
        2. The Playboy Club
========================================
    Is inactive at this time
    Broadcasts Mondays at 22:00
    Last episode: 101
    Has a rating of 6/10
========================================
        3. Two and a Half Men
========================================
    Is inactive at this time
    Broadcasts Mondays at 21:00
    Last episode: 908 (7 November 2011)
    Has a rating of 7/10
========================================
        4. How I Met Your Mother
========================================
    Is being watched at this time
    Broadcasts Mondays at 20:00
    Last episode: 719 (19 March 2012)
    Has a rating of 10/10
========================================
        5. Glee
========================================
    Is inactive at this time
    Broadcasts Tuesdays at 20:00
    Last episode: no one knows
    Has no rating
========================================
        6. New Girl
========================================
    Is being watched at this time
    Broadcasts Tuesdays at 21:00
    Last episode: 114 (14 February 2012)
    Has a rating of 8/10
========================================
        7. Raising Hope
========================================
    Is being watched at this time
    Broadcasts Tuesdays at 21:00
    Last episode: 219 (20 March 2012)
    Has a rating of 10/10
========================================
        8. Suburgatory
========================================
    Is inactive at this time
    Broadcasts Wednesdays at 20:00
    Last episode: 103 (12 October 2011)
    Has a rating of 3/10
========================================
        9. Survivor
========================================
    Is inactive at this time
    Broadcasts Wednesdays at 20:00
    Last episode: no one knows
    Has no rating
========================================
        10. Up All Night
========================================
    Is inactive at this time
    Broadcasts Wednesdays at 20:00
    Last episode: no one knows
    Has a rating of 6/10
========================================
        11. The Middle
========================================
    Is being watched at this time
    Broadcasts Wednesdays at 20:00
    Last episode: 318 (29 February 2012)
    Has a rating of 9/10
========================================
        12. Happy Endings
========================================
    Is being watched at this time
    Broadcasts Wednesdays at 21:00
    Last episode: 202
    Has a rating of 8/10
========================================
        13. Charlie's Angels
========================================
    Is inactive at this time
    Broadcasts Thursdays at 20:00
    Last episode: no one knows
    Has no rating
========================================
        14. How to Be a Gentleman
========================================
    Is inactive at this time
    Broadcasts Thursdays at 20:00
    Last episode: 104 (22 October 2011)
    Has a rating of 4/10
========================================
        15. Grey's Anatomy
========================================
    Is inactive at this time
    Broadcasts Thursdays at 21:00
    Last episode: no one knows
    Has no rating
========================================
        16. The Secret Circle
========================================
    Is inactive at this time
    Broadcasts Thursdays at 21:00
    Last episode: 102 (22 September 2011)
    Has a rating of 6/10
========================================
        17. Parks & Recreation
========================================
    Is being watched at this time
    Broadcasts Thursdays at 20:00
    Last episode: 413 (26 January 2012)
    Has a rating of 9/10
========================================
        18. 30 Rock
========================================
    Is being watched at this time
    Broadcasts Thursdays at 20:00
    Last episode: 609 (23 February 2012)
    Has a rating of 9/10
========================================
        19. Archer
========================================
    Is being watched at this time
    Broadcasts Thursdays at 22:00
    Last episode: 308 (8 March 2012)
    Has a rating of 10/10
========================================
        20. Whitney
========================================
    Is being watched at this time
    Broadcasts Wednesdays at 21:00
    Last episode: 115 (8 February 2012)
    Has a rating of 8/10
========================================
        21. The Big Bang Theory
========================================
    Is being watched at this time
    Broadcasts Thursdays at 20:00
    Last episode: 519 (8 March 2012)
    Has a rating of 9/10
========================================
        22. Fringe
========================================
    Is inactive at this time
    Broadcasts Fridays at 21:00
    Last episode: no one knows
    Has a rating of 9/10
========================================
        23. Man, Woman, Wild
========================================
    Is inactive at this time
    Broadcasts Fridays at 21:00
    Last episode: 212 (19 January 2012)
    Has a rating of 6/10
========================================
        24. Saturday Night Live
========================================
    Is being watched at this time
    Broadcasts Saturdays at 23:00
    Last episode: 3707 (19 November 2011)
    Has a rating of 8/10
========================================
        25. Boardwalk Empire
========================================
    Is inactive at this time
    Broadcasts Sundays at 21:00
    Last episode: no one knows
    Has no rating
========================================
        26. Desperate Housewives
========================================
    Is inactive at this time
    Broadcasts Sundays at 21:00
    Last episode: no one knows
    Has no rating
========================================
        27. American Dad
========================================
    Is being watched at this time
    Broadcasts Sundays at 21:00
    Last episode: 708 (11 December 2011)
    Has a rating of 8/10
========================================
        28. The Simpsons
========================================
    Is being watched at this time
    Broadcasts Sundays at 20:00
    Last episode: 2307 (27 November 2011)
    Has a rating of 7/10
========================================
        29. Family Guy
========================================
    Is being watched at this time
    Broadcasts Sundays at 21:00
    Last episode: 1013 (29 January 2012)
    Has a rating of 8/10
========================================
        30. The Cleveland Show
========================================
    Is being watched at this time
    Broadcasts Sundays at 20:00
    Last episode: 303
    Has a rating of 7/10
========================================
        31. Top Gear
========================================
    Is inactive at this time
    Broadcasts Sundays at 20:00
    Last episode: 1807 (11 March 2012)
    Has a rating of 10/10
========================================
        32. My Name Is Earl
========================================
    Is inactive at this time
    Broadcasts Thursdays at 20:00
    Last episode: 427 (14 May 2009)
    Has a rating of 9/10
========================================
        33. Top Gear America
========================================
    Is being watched at this time
    Broadcasts Tuesdays at 0:00
    Last episode: 301 (12 February 2012)
    Has a rating of 7/10
========================================
        34. Mary Tyler Moore Show
========================================
    Is being watched at this time
    Broadcasts Saturdays at 21:00
    Last episode: 312 (2 December 1972)
    Has a rating of 7/10
========================================
        35. Doctor Who
========================================
    Is being watched at this time
    Broadcasts Saturdays at 22:00
    Last episode: 613 (1 October 2011)
    Has a rating of 10/10
========================================
        36. Little Mosque on the Prairie
========================================
    Is inactive at this time
    Broadcasts Saturdays at 19:00
    Last episode: 513 (28 March 2011)
    Has a rating of 7/10
========================================
        37. Breaking In
========================================
    Is being watched at this time
    Broadcasts Fridays at 0:00
    Last episode: 105
    Has a rating of 6/10
========================================
        38. Work of Art: The Next Great Artist
========================================
    Is being watched at this time
    Broadcasts Wednesdays at 19:00
    Last episode: 201 (12 October 2011)
    Has a rating of 7/10
========================================
        39. Married With Children
========================================
    Is being watched at this time
    Broadcasts Saturdays at 21:00
    Last episode: 220 (6 March 1988)
    Has a rating of 6/10
========================================
        40. Sherlock
========================================
    Is being watched at this time
    Broadcasts Sundays at 21:00
    Last episode: 103
    Has a rating of 8/10
========================================
        41. Frasier
========================================
    Is inactive at this time
    Broadcasts Fridays at 20:00
    Last episode: 1122 (4 May 2004)
    Has a rating of 10/10
========================================
        42. Seinfeld
========================================
    Is being watched at this time
    Broadcasts Fridays at 0:00
    Last episode: 102
    Has a rating of 7/10
========================================
        43. Mad About You
========================================
    Is inactive at this time
    Broadcasts Fridays at 20:00
    Last episode: no one knows
    Has no rating
========================================
        44. Don't Trust the B. in Apartment 23
========================================
    Is being watched at this time
    Broadcasts Wednesdays at 21:00
    Last episode: 101 (11 April 2012)
    Has no rating
========================================
        45. I hate my teenage daughter
========================================
    Is being watched at this time
    Broadcasts Tuesdays at 21:00
    Last episode: 102
    Has no rating
========================================
