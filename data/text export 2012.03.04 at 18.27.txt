================================================================================
    TEXT EXPORT

    Date: Sunday, 4 March 2012, 18:27
    Number of shows: 37
    Active / inactive: 20 / 16
    Over 7 day(s)
    Shows per day: average 5.29 (range 3-10)
    Broadcasting hour: average 18:23 (range 0-23)
    Rating: average 7.50 (range 3-10)
    Oldest last watched episode: 18 November 1972 (Episode 310 from Mary Tyler Moore Show)
    Newest last watched episode: 26 February 2012 (Episode 1805 from Top Gear)
    Average date of last watched episode: 31 March 2009
    Photos (total size): 29 (/36, 3.15 MB)
    Photo sizes: average 111.14 KB (range 36.50 KB-267.58 KB)
    Memory used for one show: 152 B
    Memory used for all data: 9.57 KB
================================================================================

========================================
        1. 2 Broke Girls
========================================
    Is being watched at this time
    Broadcasts Mondays at 20:00
    Last episode: 106 (24 October 2011)
    Has a rating of 5/10
========================================
        2. The Playboy Club
========================================
    Is inactive at this time
    Broadcasts Mondays at 22:00
    Last episode: 101
    Has no rating
========================================
        3. Two and a Half Men
========================================
    Is inactive at this time
    Broadcasts Mondays at 21:00
    Last episode: 908 (7 November 2011)
    Has a rating of 7/10
========================================
        4. How I Met Your Mother
========================================
    Is being watched at this time
    Broadcasts Mondays at 20:00
    Last episode: 717 (20 January 2012)
    Has a rating of 10/10
========================================
        5. Glee
========================================
    Is inactive at this time
    Broadcasts Tuesdays at 20:00
    Last episode: no one knows
    Has no rating
========================================
        6. New Girl
========================================
    Is being watched at this time
    Broadcasts Tuesdays at 21:00
    Last episode: 106
    Has a rating of 8/10
========================================
        7. Raising Hope
========================================
    Is being watched at this time
    Broadcasts Tuesdays at 21:00
    Last episode: 213
    Has a rating of 9/10
========================================
        8. Suburgatory
========================================
    Is inactive at this time
    Broadcasts Wednesdays at 20:00
    Last episode: 103 (12 October 2011)
    Has a rating of 3/10
========================================
        9. Survivor
========================================
    Is inactive at this time
    Broadcasts Wednesdays at 20:00
    Last episode: no one knows
    Has no rating
========================================
        10. Up All Night
========================================
    Is inactive at this time
    Broadcasts Wednesdays at 20:00
    Last episode: no one knows
    Has a rating of 6/10
========================================
        11. The Middle
========================================
    Is being watched at this time
    Broadcasts Wednesdays at 20:00
    Last episode: 314
    Has a rating of 9/10
========================================
        12. Happy Endings
========================================
    Is being watched at this time
    Broadcasts Wednesdays at 21:00
    Last episode: 202
    Has a rating of 8/10
========================================
        13. Charlie's Angels
========================================
    Is inactive at this time
    Broadcasts Thursdays at 20:00
    Last episode: no one knows
    Has no rating
========================================
        14. How to Be a Gentleman
========================================
    Is inactive at this time
    Broadcasts Thursdays at 20:00
    Last episode: 104 (22 October 2011)
    Has a rating of 4/10
========================================
        15. Grey's Anatomy
========================================
    Is inactive at this time
    Broadcasts Thursdays at 21:00
    Last episode: no one knows
    Has no rating
========================================
        16. The Secret Circle
========================================
    Is inactive at this time
    Broadcasts Thursdays at 21:00
    Last episode: no one knows
    Has no rating
========================================
        17. Parks & Recreation
========================================
    Is being watched at this time
    Broadcasts Thursdays at 20:00
    Last episode: 413 (26 January 2012)
    Has a rating of 9/10
========================================
        18. 30 Rock
========================================
    Is being watched at this time
    Broadcasts Thursdays at 20:00
    Last episode: 607
    Has a rating of 9/10
========================================
        19. Archer
========================================
    Is being watched at this time
    Broadcasts Thursdays at 22:00
    Last episode: 306
    Has a rating of 10/10
========================================
        20. Whitney
========================================
    Is being watched at this time
    Broadcasts Thursdays at 21:00
    Last episode: 111 (11 January 2012)
    Has a rating of 8/10
========================================
        21. The Big Bang Theory
========================================
    Is being watched at this time
    Broadcasts Thursdays at 20:00
    Last episode: 513
    Has a rating of 9/10
========================================
        22. Fringe
========================================
    Is inactive at this time
    Broadcasts Fridays at 21:00
    Last episode: no one knows
    Has a rating of 9/10
========================================
        23. Man, Woman, Wild
========================================
    Is inactive at this time
    Broadcasts Fridays at 21:00
    Last episode: 212 (19 January 2012)
    Has a rating of 6/10
========================================
        24. Saturday Night Live
========================================
    Is being watched at this time
    Broadcasts Saturdays at 23:00
    Last episode: 3707 (19 November 2011)
    Has a rating of 8/10
========================================
        25. Boardwalk Empire
========================================
    Is inactive at this time
    Broadcasts Sundays at 21:00
    Last episode: no one knows
    Has no rating
========================================
        26. Desperate Housewives
========================================
    Is inactive at this time
    Broadcasts Sundays at 21:00
    Last episode: no one knows
    Has no rating
========================================
        27. American Dad
========================================
    Is being watched at this time
    Broadcasts Sundays at 21:00
    Last episode: 706 (27 November 2011)
    Has a rating of 8/10
========================================
        28. The Simpsons
========================================
    Is being watched at this time
    Broadcasts Sundays at 20:00
    Last episode: 2305 (13 November 2011)
    Has a rating of 7/10
========================================
        29. Family Guy
========================================
    Is being watched at this time
    Broadcasts Sundays at 21:00
    Last episode: 1009 (11 December 2011)
    Has a rating of 8/10
========================================
        30. The Cleveland Show
========================================
    Is being watched at this time
    Broadcasts Sundays at 20:00
    Last episode: 303
    Has a rating of 7/10
========================================
        31. Top Gear
========================================
    Is being watched at this time
    Broadcasts Sundays at 20:00
    Last episode: 1805 (26 February 2012)
    Has a rating of 10/10
========================================
        32. My Name Is Earl
========================================
    Is being watched at this time
    Broadcasts Thursdays at 20:00
    Last episode: 121 (16 March 2006)
    Has a rating of 7/10
========================================
        33. Top Gear America
========================================
    Is being watched at this time
    Broadcasts Tuesdays at 0:00
    Last episode: 206 (28 August 2011)
    Has a rating of 6/10
========================================
        34. Mary Tyler Moore Show
========================================
    Is being watched at this time
    Broadcasts Saturdays at 21:00
    Last episode: 310 (18 November 1972)
    Has a rating of 7/10
========================================
        35. Doctor Who
========================================
    Is inactive at this time
    No one knows when it broadcasts
    Last episode: no one knows
    Has no rating
========================================
        36. Little Mosque on the Prairie
========================================
    Is inactive at this time
    Broadcasts Saturdays at 19:00
    Last episode: 513 (28 March 2011)
    Has a rating of 7/10
========================================
        37. Breaking In
========================================
    Is being watched at this time
    Broadcasts Fridays at 0:00
    Last episode: 102
    Has a rating of 6/10
========================================
