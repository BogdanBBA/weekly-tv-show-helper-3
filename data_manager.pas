unit data_manager;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TFM = class(TForm)
    refreshB: TButton;
    closeB: TButton;
    Label5: TLabel;
    Label6: TLabel;
    mImg: TImage;
    mImgRefB: TButton;
    lb: TListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure closeBClick(Sender: TObject);
    procedure refreshBClick(Sender: TObject);
    procedure mImgRefBClick(Sender: TObject);
    procedure mImgClick(Sender: TObject);
    procedure mImgMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FM: TFM;
  px: word;
  selItem: byte;

implementation

{$R *.dfm}

uses data_types, functii, wtvsh3_pas;

procedure TFM.closeBClick(Sender: TObject);
begin FM.Close end;

procedure TFM.FormResize(Sender: TObject);
var w, h, i: word;
begin
  w:=FM.Width; h:=FM.Height;
  closeB.Left:=w div 2-closeB.Width-3; refreshB.Left:=w div 2+3;
  mImg.Left:=lb.Left; mImg.Width:=w-63;
  lb.Height:=h-lb.Top-54; //change when you have certain panel width
  for i:=1 to 2 do with TPanel(FindComponent('Panel'+inttostr(i))) do
    begin Left:=lb.Left+lb.Width+6; Top:=lb.Top; Width:=w-Left-15-lb.Left; Height:=lb.Height end;
  mImgRefBClick(FM)
end;

procedure TFM.mImgClick(Sender: TObject);
begin
  if px<mImg.Width div 2 then selItem:=1 else selItem:=2;
  mImgRefBClick(mImg)
end;

procedure TFM.mImgMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin px:=x end;

procedure TFM.mImgRefBClick(Sender: TObject);
  procedure mImgText(fromX, toX: word; text: string; selected: boolean);
  var h, hw: word;
  begin
    h:=mImg.Height;
    with mImg.Canvas do
      begin
        if selected then Font.Color:=FM.Color else Font.Color:=clWhite;
        Font.Name:='Share'; Font.Size:=round(2*h/3); Font.Style:=[fsBold];
        hw:=TextWidth(text)
      end;
    if selected then mImg.Canvas.Brush.Color:=clWhite else mImg.Canvas.Brush.Color:=FM.Color;
    mImg.Canvas.FillRect(Rect(fromX, 0, toX, h));
    mImg.Canvas.TextOut(round(fromX+(toX-fromX)/2-hw/2), 0, text)
  end;
begin
  mImg.Picture:=nil; mImg.Canvas.Brush.Color:=clFuchsia; mImg.Canvas.FillRect(Rect(0, 0, mImg.Width+1, mImg.Height+1));
  mImgText(0, mImg.Width div 2, 'Photos', selItem=1);
  mImgText(mImg.Width div 2, mImg.Width+1, 'Music', selItem=2);
end;

procedure TFM.refreshBClick(Sender: TObject);
var i: word;
begin
  mImgRefBClick(FM);
  for i:=1 to 2 do with TPanel(FindComponent('Panel'+inttostr(i))) do Visible:=selItem=i
end;

procedure TFM.FormShow(Sender: TObject);
begin
  MessageDlg('This feature has been indefinitely postponed due to the onset of creating Video Database 4.'+nl+'      - August 24th, 2012', mtWarning, [mbOK], 0);
  if Screen.Fonts.IndexOf('Share')=-1 then showmessage('The font "Share" can not be found on your PC. It is needed. Install it and restart the program.');
  selItem:=1; refreshBClick(FM)
end;

end.
