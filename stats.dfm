object FSt: TFSt
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Statistics'
  ClientHeight = 654
  ClientWidth = 558
  Color = 9335573
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -19
  Font.Name = 'Asap'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 22
  object Label0: TLabel
    Left = 22
    Top = 8
    Width = 151
    Height = 45
    Caption = 'Statistics'
    Font.Charset = ANSI_CHARSET
    Font.Color = 6225838
    Font.Height = -40
    Font.Name = 'Share'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 22
    Top = 59
    Width = 53
    Height = 22
    Caption = 'Shows'
  end
  object Label2: TLabel
    Left = 480
    Top = 59
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 22
    Top = 87
    Width = 211
    Height = 22
    Caption = 'Of which active / inactive'
  end
  object Label4: TLabel
    Left = 480
    Top = 87
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 22
    Top = 130
    Width = 40
    Height = 22
    Caption = 'Over'
  end
  object Label6: TLabel
    Left = 480
    Top = 130
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 22
    Top = 158
    Width = 121
    Height = 22
    Caption = 'Shows per day'
  end
  object Label8: TLabel
    Left = 480
    Top = 158
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 22
    Top = 186
    Width = 154
    Height = 22
    Caption = 'Broadcasting hour'
  end
  object Label10: TLabel
    Left = 480
    Top = 186
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label11: TLabel
    Left = 22
    Top = 214
    Width = 107
    Height = 22
    Caption = 'Show ratings'
  end
  object Label12: TLabel
    Left = 480
    Top = 214
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label13: TLabel
    Left = 22
    Top = 257
    Width = 237
    Height = 22
    Caption = 'Oldest episode last watched'
  end
  object Label14: TLabel
    Left = 480
    Top = 257
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label15: TLabel
    Left = 22
    Top = 296
    Width = 244
    Height = 22
    Caption = 'Newest episode last watched'
  end
  object Label16: TLabel
    Left = 480
    Top = 296
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label14b: TLabel
    Left = 352
    Top = 275
    Width = 184
    Height = 17
    Alignment = taRightJustify
    Caption = 'Oldest episode last watched'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Asap'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label16b: TLabel
    Left = 352
    Top = 318
    Width = 184
    Height = 17
    Alignment = taRightJustify
    Caption = 'Oldest episode last watched'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Asap'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label17: TLabel
    Left = 22
    Top = 539
    Width = 229
    Height = 22
    Caption = 'Memory used for one show'
  end
  object Label18: TLabel
    Left = 480
    Top = 539
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label19: TLabel
    Left = 22
    Top = 567
    Width = 212
    Height = 22
    Caption = 'Memory used for all data'
  end
  object Label20: TLabel
    Left = 480
    Top = 567
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label21: TLabel
    Left = 22
    Top = 341
    Width = 240
    Height = 22
    Caption = 'Avg. date of last watched ep.'
  end
  object Label22: TLabel
    Left = 480
    Top = 341
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label0b: TLabel
    Left = 367
    Top = 634
    Width = 181
    Height = 17
    Alignment = taRightJustify
    Caption = '*showing stats for all shows'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Asap'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label23: TLabel
    Left = 22
    Top = 369
    Width = 151
    Height = 22
    Caption = 'Photos (total size)'
  end
  object Label24: TLabel
    Left = 480
    Top = 369
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label25: TLabel
    Left = 22
    Top = 412
    Width = 95
    Height = 22
    Caption = 'Photo sizes'
  end
  object Label26: TLabel
    Left = 480
    Top = 412
    Width = 56
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label27: TLabel
    Left = 22
    Top = 440
    Width = 203
    Height = 22
    Caption = 'Theme songs (total size)'
  end
  object Label28: TLabel
    Left = 470
    Top = 440
    Width = 66
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label28'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label29: TLabel
    Left = 22
    Top = 483
    Width = 147
    Height = 22
    Caption = 'Theme song sizes'
  end
  object Label30: TLabel
    Left = 470
    Top = 483
    Width = 66
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label30'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label31: TLabel
    Left = 22
    Top = 511
    Width = 173
    Height = 22
    Caption = 'Total utility files size'
  end
  object Label32: TLabel
    Left = 470
    Top = 511
    Width = 66
    Height = 22
    Alignment = taRightJustify
    Caption = 'Label32'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button1: TButton
    Left = 200
    Top = 595
    Width = 161
    Height = 52
    Cursor = crHandPoint
    Caption = 'Ok'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Asap'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = Button1Click
  end
  object pb1: TProgressBar
    Left = 22
    Top = 115
    Width = 514
    Height = 9
    Smooth = True
    SmoothReverse = True
    TabOrder = 1
  end
  object pb2: TProgressBar
    Left = 22
    Top = 242
    Width = 514
    Height = 9
    Max = 1000
    Smooth = True
    SmoothReverse = True
    TabOrder = 2
  end
  object pb3: TProgressBar
    Left = 22
    Top = 397
    Width = 514
    Height = 9
    Max = 1000
    Smooth = True
    SmoothReverse = True
    TabOrder = 3
  end
  object pb4: TProgressBar
    Left = 22
    Top = 468
    Width = 514
    Height = 9
    Max = 1000
    Smooth = True
    SmoothReverse = True
    TabOrder = 4
  end
end
