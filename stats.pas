unit stats;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TFSt = class(TForm)
    Button1: TButton;
    Label0: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    pb1: TProgressBar;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    pb2: TProgressBar;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label14b: TLabel;
    Label16b: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label0b: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    pb3: TProgressBar;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    pb4: TProgressBar;
    Label31: TLabel;
    Label32: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSt: TFSt;

implementation

{$R *.dfm}

uses data_types, functii;

procedure TFSt.FormShow(Sender: TObject);
var x: int64;
begin
  get_data_ready;
  Label2.Caption:=inttostr(st.ns);
  Label4.Caption:=inttostr(st.nas)+' / '+inttostr(st.nias);
  pb1.Min:=0; pb1.Max:=st.ns; pb1.Position:=st.nas;
  if st.nd=1 then Label6.Caption:='1 day' else Label6.Caption:=inttostr(st.nd)+' days';
  Label8.Caption:=formatfloat('"avg. "0.00" (range "', st.avgSpD)+inttostr(st.minSpD)+'-'+inttostr(st.maxSpD)+')';
  Label10.Caption:='avg. '+inttostr(trunc(st.avgBH))+':'+formatfloat('00" (range "', frac(st.avgBH)*60)+inttostr(st.minBH)+'-'+inttostr(st.maxBH)+')';
  Label12.Caption:=formatfloat('"avg. "0.00" (range "', st.avgR)+inttostr(st.minR)+'-'+inttostr(st.maxR)+')';
  pb2.Position:=round(st.avgR*100);
  Label14.Caption:=formatdatetime('d mmmm yyyy', st.olwep); Label14b.Caption:=st.olwepSTR;
  Label16.Caption:=formatdatetime('d mmmm yyyy', st.nlwep); Label16b.Caption:=st.nlwepSTR;
  Label18.Caption:=tkb(st.mem1S);
  Label20.Caption:=tkb(st.memAllData);
  Label22.Caption:=formatdatetime('d mmmm yyyy', st.avgLWEp);
  Label24.Caption:=inttostr(st.nph)+' (/'+inttostr(st.ns)+', '+tkb(st.tPhSz)+')';
  pb3.Min:=0; pb3.Max:=st.ns; pb3.Position:=st.nph;
  Label26.Caption:='avg. '+tkb(st.avgPhSz)+' (range '+tkb(st.minPhSz)+'-'+tkb(st.maxPhSz)+')';
  Label28.Caption:=inttostr(st.nTS)+' (/'+inttostr(st.ns)+', '+tkb(st.tTSS)+')';
  pb4.Min:=0; pb4.Max:=st.ns; pb4.Position:=st.nTS;
  Label30.Caption:='avg. '+tkb(st.avgTSS)+' (range '+tkb(st.minTSS)+'-'+tkb(st.maxTSS)+')';
  x:=0; if st.nph+st.nTS<>0 then x:=round((st.tPhSz+st.tTSS)/(st.nph+st.nTS));
  Label32.Caption:=tkb(st.tPhSz+st.tTSS)+' ('+inttostr(st.nph+st.nTS)+' files, avg. '+tkb(x)+')'
  //perhaps, for the future, range and average theme song length?
end;

procedure TFSt.Button1Click(Sender: TObject);
begin
  FSt.Close
end;

end.
