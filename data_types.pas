unit data_types;
// note: on 3.5 update, memory utilized dropped from ~7.5 KB to ~1.5 KB (probably neither were precise)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Xml.VerySimple, DateUtils;

const
  nl=#13#10;
  dnl=nl+nl;

  n_necessary_files=7;
  necessary_files: array[1..n_necessary_files] of string=
    ('data\shows.xml', 'data\themes\list.thl', 'data\themes\default.th', 'data\logo1.png', 'data\logo1.png', 'data\star.png', 'data\starG.png');

type TShow=record
  name: string;
  dow, broadcast_hour, rating: byte;
  last_episode: word;
  last_episode_date: TDate;
  last_episode_modified: TDateTime;
  last_episode_title, custom_epg_address, custom_epg_format: string;
  active: boolean;
end;

type TDoW=record
  name, name_short: string;
  shows: TStringList;
end;

type TStat=record
  ns, nas, nias, nd, nph, nTS, minSpD, maxSpD, minR, maxR, minBH, maxBH: word;
  mem1S, memAllData, minPhSz, maxPhSz, tPhSz, avgPhSz, minTSS, maxTSS, tTSS, avgTSS: int64;
  avgR, avgBH, avgSpD: real;
  nlwep, olwep, avgLWEp: TDate;
  nlwepSTR, olwepSTR: string;
end;

type TTheme=record
  colors: record
    form, lbs: TColor;
    end;
  fonts: record
    title, subtitle_date, subtitle_ep, today_is, today_label, ldays, lbs: string[255];
    end;
end;

type TSettings=record
  playonstartup, playonshowrefresh, seeWeekMondaytoSunday: boolean;
  lastEpisodeFormatting, lastEpisodeFormatting2: string; //initialized internally for now
end;

type TSel=record
  show, day: word;
end;

var
  s: array of TShow;
  d: array[0..7] of TDow;
  sel: TSel;
  sx: TXMLVerySimple;

  ns: word;
  dow, curr_dow: byte;
  dateX: TDate;

  st: TStat;

  theme1_name: string;
  theme1: TTheme;

  sett: TSettings;

  f: textfile;
  fth: file of TTheme;

procedure log(s: string);
procedure read_settings(also_implement: boolean);
procedure save_settings;
procedure init_read_data;
procedure save_data;
procedure get_data_ready;
procedure load_theme(th_name: string; var dest_var: TTheme);
procedure save_theme(name: string; th: TTheme);
procedure read_version2_compatible_text_data;
function show_pos_in_s(Aname: string): word;

implementation

uses functii, wtvsh3_pas;

procedure log(s: string);
var logf: textfile;
begin
  try
    F1.Label1.Caption:=s+formatdatetime(' (d mmm yyyy hh:nn)', now);
    assignfile(logf, 'data\log.txt'); append(logf); writeln(logf, formatdatetime('dd.mmm.yyyy hh:nn:ss    |    ', now), s); closefile(logf)
  except
    begin
      try
        F1.Label1.Caption:=s+formatdatetime('"(Log retry, "d mmm yyyy hh:nn)', now);
        assignfile(logf, 'data\log.txt'); append(logf); writeln(logf, formatdatetime('dd.mmm.yyyy hh:nn:ss    |    "Log retry: "', now), s); closefile(logf)
      except on E:Exception do showmessage('Failed a second time to log message "'+s+'"'+dnl+e.classname+' :  '+e.Message) end
    end
  end
end;

procedure read_settings(also_implement: boolean);
var x: word;
begin
  log('Reading settings with also_implement='+booleantotruefalse(also_implement)); assignfile(f, 'data\settings.txt'); reset(f);
  readln(f, x); sett.playonstartup:=inttoboolean(x); readln(f, x); sett.playonshowrefresh:=inttoboolean(x);
  readln(f, x); sett.seeWeekMondaytoSunday:=inttoboolean(x); closefile(f);
  if also_implement then begin F1.Playonstartup1.Checked:=sett.playonstartup; F1.Playonshowclick1.Checked:=sett.playonshowrefresh; F1.AlwaysSeeWeekMonSun1.Checked:=sett.seeWeekMondaytoSunday; F1.AlwaysSeeWeekMonSun1Click(F1.Button1); F1.AlwaysSeeWeekMonSun1Click(F1.Button1) end
end;

procedure save_settings;
begin
  log('Saving settings...'); assignfile(f, 'data\settings.txt'); rewrite(f);
  writeln(f, integer(sett.playonstartup)); writeln(f, integer(sett.playonshowrefresh)); writeln(f, integer(sett.seeWeekMondaytoSunday));
  closefile(f)
end;

procedure init_read_data;
  {*}function Convert_LEpD(s: string): TDate;
     var y, m, d: word;
     begin
       try d:=strtoint(s[1]+s[2]); m:=strtoint(s[4]+s[5]); y:=strtoint(copy(s, 7, 4)); result := encodedate(y, m, d)
       except result := dateX end
     end;
  {*}function Convert_LEpDT(s: string): TDateTime;
     var y, m, d, h, n: word;
     begin
       try d:=strtoint(s[1]+s[2]); m:=strtoint(s[4]+s[5]); y:=strtoint(copy(s, 7, 4)); h:=strtoint(s[12]+s[13]); n:=strtoint(s[15]+s[16]);
       result := encodedatetime(y, m, d, h, n, 0, 0)
       except result := dateX end
     end;
var i: word; phz: string;
begin
  try
    log('Initializing data...'); phz:='Data existance checks'; log('Phase "'+phz+'"...');
    if not directoryexists('data') then begin showmessage('The "data" folder does not exist. That''s an error.'); exit end;
    if not directoryexists('data\themes') then begin showmessage('The "data\themes" folder does not exist. That''s an error.'); exit end;
    if not directoryexists('data\photos') then begin showmessage('The "data\photos" folder does not exist. That''s an error.'); exit end;
    if not directoryexists('data\music') then if not createdirectory('data\music', nil) then showmessage('The "data\music" couldn''t be created. That can be a problem. Continuing...');
    phz:='Phase "reading settings"...'; log(phz);
    if not fileexists('data\settings.txt') then
      begin log('The "data\settings.txt" file does not exist. Applying default settings....'); sett.playonstartup:=false; sett.playonshowrefresh:=false end
    else read_settings(true);
    sett.lastEpisodeFormatting:='S%d/E%d'; sett.lastEpisodeFormatting2:='%d:%d';
    for i:=1 to n_necessary_files do if not fileexists(necessary_files[i]) then
      begin showmessage('The file "'+necessary_files[i]+'" does not exist. That''s an error.'); exit end;
    //more or less temp
    phz:='Loading startup theme'; log('Phase "'+phz+'"...');
    assignfile(f, 'data\themes\list.thl'); reset(f); readln(f, theme1_name); closefile(f);
    log('Loading default theme "'+theme1_name+'"...'); load_theme(theme1_name, theme1);
      //phz:='Reading old data'; log('Phase "'+phz+'"...'); read_version2_compatible_text_data; save_data;
    //
    phz:='DoW StringList Create'; log('Phase "'+phz+'"...');
    d[0].name:='unknown'; d[0].name_short:='idk'; d[0].shows:=TStringList.Create; d[0].shows.casesensitive:=true; d[0].shows.Sorted:=false;
    for i:=1 to 7 do
      begin
        d[i].name:=FormatSettings.longdaynames[dayofweek(encodedate(2007, 1, i))]; //showmessage('dow i='+inttostr(i)+', name='+d[i].name);
        d[i].name_short:=FormatSettings.shortdaynames[dayofweek(encodedate(2007, 1, i))];
        d[i].shows:=TStringList.Create;
        d[i].shows.CaseSensitive:=false;
        d[i].shows.Sorted:=false
      end;
    dow:=prevd(dayofweek(now)); curr_dow:=dow; log('dow = '+inttostr(dow)+' = '+d[dow].name);
    //
    sel.show:=0; sel.day:=0; dateX:=encodedate(1899,12,30);
    log('Reading data...'); phz:='Creating and reading XML'; log('Phase "'+phz+'"...');
    sx:=TXMLVerySimple.Create; sx.LoadFromFile('data\shows.xml'); ns:=sx.Root.ChildNodes.Count; setlength(s, ns+1);
    phz:='Converting ('+inttostr(ns)+') shows from XML'; log('Phase "'+phz+'"...');
    for i:=0 to ns-1 do
      begin
        try s[i+1].name := sx.Root.ChildNodes[i].Find('name').Text                                         except s[i+1].name:='unknown?!' end;
        try s[i+1].active := truefalsetoboolean(sx.Root.ChildNodes[i].Attribute['active'], true)           except s[i+1].active:=false end;
        try s[i+1].dow := strtoint(sx.Root.ChildNodes[i].Find('day_of_week').Text)                         except s[i+1].dow:=0 end;
        try s[i+1].broadcast_hour := strtoint(sx.Root.ChildNodes[i].Find('broadcast_hour').Text)           except s[i+1].broadcast_hour:=0 end;
        try s[i+1].last_episode := strtoint(sx.Root.ChildNodes[i].Find('last_episode').Text)               except s[i+1].last_episode:=0 end;
        try s[i+1].last_episode_date := Convert_LEpD(sx.Root.ChildNodes[i].Find('last_episode_date').Text) except s[i+1].last_episode_date:=dateX end;
        try s[i+1].last_episode_modified := Convert_LEpDT(sx.Root.ChildNodes[i].Attribute['LEp_modified']) except s[i+1].last_episode_modified:=dateX end;
        try s[i+1].last_episode_title := sx.Root.ChildNodes[i].Find('last_episode_title').Text             except s[i+1].last_episode_title:='unknown' end;
        try s[i+1].custom_epg_address := sx.Root.ChildNodes[i].Find('custom_epg_address').Text             except s[i+1].custom_epg_address:='' end;
        try s[i+1].custom_epg_format := sx.Root.ChildNodes[i].Find('custom_epg_format').Text               except s[i+1].custom_epg_format:='' end;
        try s[i+1].rating := strtoint(sx.Root.ChildNodes[i].Find('rating').Text)                           except s[i+1].rating:=0 end;
      end;
    log('Read data successfully!');
  except on E:Exception do showmessage('An error was encountered while initializing and reading data, phase="'+phz+'"'+dnl+E.ClassName+' error raised, with message : '+E.Message+dnl+'Restart the program, restore data from backup file and do some programming digging.') end
end;

procedure save_data;
var i: word;
begin
  try
  log('Backing up XML file...');
  if fileexists('data\shows older.xml') then begin sx.LoadFromFile('data\shows older.xml'); sx.SaveToFile('data\shows oldest.xml') end;
  sx.LoadFromFile('data\shows.xml'); sx.SaveToFile('data\shows older.xml');
  log('Saving data (XML shows)...');
  sx.Clear; sx.Root.NodeName:='WTvSH3_Show_Database'; sx.Root.SetAttribute('DateSaved', formatdatetime('dddd, mmmm d, yyyy, hh:nn', now));
  for i:=0 to ns-1 do
    begin
      sx.Root.AddChild('show'); sx.Root.ChildNodes[i].SetAttribute('ID', inttostr(i+1)); sx.Root.ChildNodes[i].SetAttribute('active', booleantotruefalse(s[i+1].Active));
      with sx.Root.ChildNodes[i] do
        begin
          if s[i+1].last_episode_modified<>dateX then SetAttribute('LEp_modified', formatdatetime('dd.mm.yyyy hh:nn', s[i+1].last_episode_modified));
          AddChild('name'); Find('name').Text:=s[i+1].name;
          AddChild('day_of_week'); Find('day_of_week').Text:=inttostr(s[i+1].dow);
          AddChild('broadcast_hour'); Find('broadcast_hour').Text:=inttostr(s[i+1].broadcast_hour);
          AddChild('last_episode'); Find('last_episode').Text:=inttostr(s[i+1].last_episode);
          if s[i+1].last_episode_date<>dateX then
            begin AddChild('last_episode_date'); Find('last_episode_date').Text:=formatdatetime('dd/mm/yyyy', s[i+1].last_episode_date) end;
          AddChild('last_episode_title').Text:=s[i+1].last_episode_title;
          AddChild('custom_epg_address').Text:=s[i+1].custom_epg_address;
          AddChild('custom_epg_format').Text:=s[i+1].custom_epg_format;
          AddChild('rating'); Find('rating').Text:=inttostr(s[i+1].rating)
        end;
    end;
  sx.SaveToFile('data\shows.xml')
  except on E:Exception do showmessage('An error was encountered while saving XML data. Ouch! See the backups in the "\data" folder; don''t save anything else until you sort this out or risk having those backups deleted!'+dnl+E.ClassName+' error raised, with message : '+E.Message) end
end;

procedure get_data_ready; //                                                  ::
var i, j, k, stR, stRn, stBH, stDn: word; phz, fn: string; ok: boolean; stDt: TDate; sr: TSearchRec;
begin
  try
    log('Gettings daily show list...');
    phz:='Clearing d[1..7].shows TStringList'; for i:=0 to 7 do d[i].shows.Clear;
    phz:='Filtering (currently, only by name) and adding show names to DoW show list';
    for i:=0 to ns do
      if (strtocase(F1.srcE.Text, [''{, ' ', '  ', '   '}])<>0) or (pos(ansiuppercase(F1.srcE.Text), ansiuppercase(s[i].name))<>0) then
        if s[i].dow in [0..7] then
          begin
            d[s[i].dow].shows.Add(s[i].name)
          end;
    //sort
    log('Sorting shows...'); phz:='sorting shows';
    for i:=1 to 7 do
      with d[i] do
        begin
          //log('sorting day='+inttostr(i)+', it has '+inttostr(shows.Count)+' shows');
          if shows.Count>1 then
            begin
              for j:=0 to shows.Count-2 do
                for k:=j+1 to shows.Count-1 do
                  begin
                    phz:='sorting shows(day='+inttostr(i)+',j='+inttostr(j)+',k='+inttostr(k)+')'; ok:=false; //log(phz);
                    if F1.sortM1.Checked then ok := ansiuppercase(s[show_pos_in_s(shows[j])].name) > ansiuppercase(s[show_pos_in_s(shows[k])].name)
                    else if F1.sortM2.Checked then ok := s[show_pos_in_s(shows[j])].last_episode < s[show_pos_in_s(shows[k])].last_episode
                    else if F1.sortM3.Checked then ok := s[show_pos_in_s(shows[j])].last_episode_date < s[show_pos_in_s(shows[k])].last_episode_date
                    else if F1.sortM4.Checked then ok := s[show_pos_in_s(shows[j])].last_episode_modified < s[show_pos_in_s(shows[k])].last_episode_modified
                    else if F1.sortM5.Checked then ok := s[show_pos_in_s(shows[j])].broadcast_hour > s[show_pos_in_s(shows[k])].broadcast_hour
                    else if F1.sortM6.Checked then ok := s[show_pos_in_s(shows[j])].rating < s[show_pos_in_s(shows[k])].rating
                    else begin showmessage('get_data_ready ERROR: sorting: nothing is selected (Sort menu)! WTF!?! (Exiting)'); exit end;
                    //if F1.sortM3.Checked then log('ep_date_sort: (day='+inttostr(i)+', x='+inttostr(j+1)+', y='+inttostr(k+1)+'), OK = '+booleantostr(ok)+' = '+formatdatetime('yyyy.mmm.dd', s[show_pos_in_s(shows[j])].last_episode_date)+' < '+formatdatetime('yyyy.mmm.dd', s[show_pos_in_s(shows[k])].last_episode_date));
                    if ok then shows.Exchange(j,k)
                  end
            end
        end;
    //stats
    log('Getting stats...'); phz:='getting stats'; if ns=0 then begin showmessage('ns=0, exiting getting_stats section of get_data_ready...'); exit end;
    st.mem1S:=sizeof(TShow); st.memAllData:=sizeof(TShow)*ns+sizeof(TDoW)*8+sizeof(sel)+sizeof(st)+4+sizeof(theme1)+sizeof(theme1_name)+sizeof(dateX)+sizeof(f)+sizeof(fth);
    st.nas:=0; st.nias:=0; st.nd:=integer(d[1].shows.Count>0);
    st.minSpD:=d[1].shows.Count; st.maxSpD:=st.minSpD; st.minBH:=s[1].broadcast_hour; st.maxBH:=st.minBH;
    stBH:=0; stDt:=0; stDn:=0; st.olwepSTR:='---'; st.nlwepSTR:='---';
    st.nph:=0; st.tPhSz:=0; st.minPhSz:=0; st.maxPhSz:=0; st.avgPhSz:=0; st.nTS:=0; st.tTSS:=0; st.minTSS:=0; st.maxTSS:=0; st.avgTSS:=0;
    if s[1].last_episode_date>=encodedate(1901,1,1) then
      begin st.olwep:=s[1].last_episode_date; st.nlwep:=s[1].last_episode_date end else begin st.olwep:=now; st.nlwep:=dateX end;

    if s[1].last_episode_date<>dateX then begin stDt:=s[1].last_episode_date; stDn:=1 end;
    if s[1].rating<>0 then begin stR:=s[1].rating; st.minR:=stR; st.maxR:=stR; stRn:=1 end else begin stR:=0; stRn:=0; st.minR:=10; st.maxR:=0 end;
    if s[1].active then st.nas:=1 else st.nas:=0;

    for i:=2 to 7 do
      begin
        inc(st.nd, integer(d[i].shows.Count>0));
        if d[i].shows.Count < st.minSpD then st.minSpD:=d[i].shows.Count;
        if d[i].shows.Count > st.maxSpD then st.maxSpD:=d[i].shows.Count
      end;
    for i:=2 to ns do
      begin
        if s[i].active then inc(st.nas) else inc(st.nias);
        if s[i].broadcast_hour < st.minBH then st.minBH:=s[i].broadcast_hour;
        if s[i].broadcast_hour > st.maxBH then st.maxBH:=s[i].broadcast_hour;
        inc(stBH, s[i].broadcast_hour);
        if s[i].rating<>0 then
          begin
            if s[i].rating < st.minR then st.minR:=s[i].rating;
            if s[i].rating > st.maxR then st.maxR:=s[i].rating;
            inc(stR, s[i].rating); inc(stRn)
          end;
        if s[i].last_episode_date>=encodedate(1901,1,1) then
          begin                     //showmessage(s[i].name+' last ep date is OK ='+nl+formatdatetime('yyyy.mm.dd', s[i].last_episode_date));
            if s[i].last_episode_date < st.olwep then begin {showmessage(s[i].name+' last ep date = '+nl+formatdatetime('yyyy.mm.dd', s[i].last_episode_date)+' < minDate = '+formatdatetime('yyyy.mm.dd', st.olwep)); }st.olwep:=s[i].last_episode_date; st.olwepSTR:='Episode '+inttostr(s[i].last_episode)+' from '+s[i].name end;
            if s[i].last_episode_date > st.nlwep then begin st.nlwep:=s[i].last_episode_date; st.nlwepSTR:='Episode '+inttostr(s[i].last_episode)+' from '+s[i].name end;
            stDt:=stDt+s[i].last_episode_date; inc(stDn)
          end
      end;
    for i:=1 to ns do
      begin
        fn:='data\photos\'+s[i].name+'.jpg';
        if fileexists(fn) then
          begin
            findfirst(fn, 0, sr); inc(st.tPhSz, sr.Size); inc(st.nph);
            if st.maxPhSz < sr.Size then st.maxPhSz:=sr.Size;
            if st.minPhSz=0 then st.minPhSz:=sr.Size else if st.minPhSz > sr.Size then st.minPhSz:=sr.Size
          end;
        fn:='data\music\'+s[i].name+'.mp3';
        if fileexists(fn) then
          begin
            findfirst(fn, 0, sr); inc(st.tTSS, sr.Size); inc(st.nTS);
            if st.maxTSS < sr.Size then st.maxTSS:=sr.Size;
            if st.minTSS=0 then st.minTSS:=sr.Size else if st.minTSS > sr.Size then st.minTSS:=sr.Size
          end;
      end;
    st.ns:=st.nas+st.nias;
    st.avgSpD:=ns/7; st.avgBH:=stBH/ns;
    if stRn<>0 then st.avgR:=stR/stRn else st.avgR:=0;
    st.avgLWEp:=stDt/stDn;
    if st.nph<>0 then st.avgPhSz:=round(st.tPhSz/st.nph);
    if st.nTS<>0 then st.avgTSS:=round(st.tTSS/st.nTS);
    //
  except on E:Exception do begin showmessage('An error was encountered while getting data ready (phase="'+phz+'")...'+dnl+'What happens next is that probably you''ll get an error.'+nl+'Check "log.txt" for info'+dnl+E.ClassName+' error raised, with message : '+E.Message); log('get_data_ready ERROR, phase="'+phz+'"') end end
end;

procedure load_theme(th_name: string; var dest_var: TTheme);
begin
  log('Loading theme "'+th_name+'"...');
  try
  assignfile(fth, 'data\themes\'+th_name+'.th'); filemode:=fmopenread; reset(fth); read(fth, dest_var); closefile(fth); theme1_name:=th_name;
  except on E:Exception do showmessage('An error was encountered while loading theme.'+nl+'Check "log.txt" for info'+dnl+E.ClassName+' error raised, with message : '+E.Message) end;
  log('Success. Will NOT be applied from here (load_theme()).');
end;

procedure save_theme(name: string; th: TTheme);
begin
  log('Saving theme "'+name+'"...'); assignfile(fth, 'data\themes\'+name+'.th'); rewrite(fth); write(fth, th); closefile(fth)
end;

procedure read_version2_compatible_text_data;
//var i: word; auxs: string;
begin
  try
  log('Reading data compatible with text version 2...'); assignfile(f, 'data\datafor3.txt'); reset(f); ns:=0;
  while not eof(f) do
    begin log('About to increment ns to '+inttostr(ns+1));
      inc(ns); setlength(s, ns+1);
      log('to read name'); readln(f, s[ns].name); log('Reading "'+s[ns].name+'"..'); log('to read dow'); readln(f, s[ns].dow);
      log('to read H'); readln(f, s[ns].broadcast_hour);
      log('to read ep'); readln(f, s[ns].last_episode); log('to read ep_date'); readln(f, s[ns].last_episode_date);
      s[ns].rating:=0; s[ns].active:=true;
    end;
  closefile(f); log('Reading data compatible with text version 2: Success!')
  except on E:Exception do showmessage('An error was encountered while read ing old data.'+dnl+E.ClassName+' error raised, with message : '+E.Message) end
end;

function show_pos_in_s(Aname: string): word;
var x: word;
begin
  result:=0; for x:=1 to ns do if s[x].name=Aname then break;
  if x<>0 then result:=x else showmessage('show_pos_in_s ERROR: cannot find show name ="'+Aname+'"')
end;

end.
