program wtvsh3;

uses
  Forms,
  wtvsh3_pas in 'wtvsh3_pas.pas' {F1},
  thU in 'thU.pas' {ThForm},
  data_types in 'data_types.pas',
  show_editor in 'show_editor.pas' {FEd},
  shows in 'shows.pas' {FS},
  thE in 'thE.pas' {ThEd},
  stats in 'stats.pas' {FSt},
  about in 'about.pas' {FAb},
  data_manager in 'data_manager.pas' {FM},
  Help2 in 'Help2.pas' {FH};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TF1, F1);
  Application.CreateForm(TThForm, ThForm);
  Application.CreateForm(TFEd, FEd);
  Application.CreateForm(TFS, FS);
  Application.CreateForm(TThEd, ThEd);
  Application.CreateForm(TFSt, FSt);
  Application.CreateForm(TFAb, FAb);
  Application.CreateForm(TFM, FM);
  Application.CreateForm(TFH, FH);
  Application.Run;
end.
