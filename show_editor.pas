unit show_editor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, DateUtils;

type
  TFEd = class(TForm)
    Label1: TLabel;
    Label5: TLabel;
    cha: TCheckBox;
    Panel1: TPanel;
    eh: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    enm: TEdit;
    cb1: TComboBox;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    chd: TCheckBox;
    elep: TEdit;
    dtp: TDateTimePicker;
    Label8: TLabel;
    cb2: TComboBox;
    Button1: TButton;
    Button2: TButton;
    Timer1: TTimer;
    Button3: TButton;
    Button4: TButton;
    els: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    procedure chaClick(Sender: TObject);
    procedure enmChange(Sender: TObject);
    procedure chdClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure cb1Change(Sender: TObject);
    procedure elepChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEd: TFEd;
  dow_changed: boolean;
  oldep, newep: word;
  oldn, newn: string;

implementation

{$R *.dfm}

uses data_types, wtvsh3_pas, functii, shows;

procedure TFEd.Button1Click(Sender: TObject);
//var i: byte;
begin
  FEd.Close;
  if dow_changed then begin F1.refresh_bClick(Button1); lb[dow_corr(s[sel.show].dow)].ItemIndex:=lb[dow_corr(s[sel.show].dow)].Items.IndexOf(lb_show_format(sel.show)) end
  else lb[dow_corr(s[sel.show].dow)].Items[lb[dow_corr(s[sel.show].dow)].ItemIndex]:=lb_show_format(sel.show)
end;

procedure TFEd.cb1Change(Sender: TObject);
begin dow_changed:=true end;

procedure TFEd.chaClick(Sender: TObject);
begin
  Panel1.Enabled:=cha.Checked
end;

procedure TFEd.chdClick(Sender: TObject);
begin
  dtp.Enabled:=not chd.Checked
end;

procedure TFEd.elepChange(Sender: TObject);
begin try newep:=strtoint(els.Text)*1000+strtoint(elep.Text) except newep:=0 end end;

procedure TFEd.enmChange(Sender: TObject);
begin
  if (enm.Text<>'') and (enm.Text<>' ') then Label5.Caption:=enm.Text
  else begin Label5.Caption:='Show must have a name! :)'; exit end;
  newn:=enm.Text
end;

procedure TFEd.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=false; Button1Click(Timer1)
end;

function existing_rename_ok(o, n: string): boolean;
begin
  result:=true;
  if fileexists(o) then if not renamefile(o, n) then result:=MessageDlg('Renaming of file "'+o+'" into "'+n+'" has failed.'+dnl+'Would you like to continue saving?', mtWarning, mbOKCancel, 0)=mrOK
end;

procedure TFEd.Button2Click(Sender: TObject);
begin
  try
  log('Attempting to save data for show "'+enm.Text+'"...');
  if ((enm.Text='') or (enm.Text=' ')) then begin showmessage('Type a valid show name!'); exit end;
  if ((eh.Text='') or (eh.Text=' ')) then begin showmessage('Type a valid broadcast hour!'); exit end;
  if ((elep.Text='') or (elep.Text=' ')) then begin showmessage('Type a valid episode number (or 0, in case you don''t know any)!'); exit end;
  //
  if oldn<>newn then
    begin
      if not existing_rename_ok('data\photos\'+oldn+'.jpg', 'data\photos\'+newn+'.jpg') then exit;
      if not existing_rename_ok('data\music\'+oldn+'.mp3', 'data\music\'+newn+'.mp3') then exit
    end;
  s[sel.show].name:=enm.Text; s[sel.show].active:=cha.Checked;
  s[sel.show].broadcast_hour:=strtoint(eh.Text);
  s[sel.show].last_episode:=strtoint(els.Text)*1000+strtoint(elep.Text);
  if newep<>oldep then s[sel.show].last_episode_modified:=now;
  s[sel.show].dow:=cb1.ItemIndex; s[sel.show].rating:=cb2.ItemIndex;
  if chd.Checked then s[sel.show].last_episode_date:=dateX else s[sel.show].last_episode_date:=dtp.Date;
  //
  save_data; Button2.Enabled:=false; Button2.Caption:='Saved!'; Timer1.enabled:=true
  except on E:Exception do showmessage(e.Message+' raised an error with the message "'+e.Message+'". What have you done?!') end
end;

procedure TFEd.Button3Click(Sender: TObject);
var x: word;
begin
  try x:=strtoint(els.Text)*1000+strtoint(elep.Text) except exit end;
  if x=0 then begin elep.Text:='1'; els.Text:='1' end
  else begin elep.Text:=inttostr(strtoint(elep.Text)+1); dtp.Date:=incday(dtp.Date, 7) end;
  chd.Checked:=true
end;

procedure TFEd.Button4Click(Sender: TObject);
var p: TPoint;
begin getcursorpos(p); F1.PopupMenu2.Popup(p.X, p.Y) end;

procedure TFEd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if pos('d', Button2.Caption)<>0 then
    begin
      if FS.Showing then begin FS.ref_ed_bClick(FEd); FS.lb.ItemIndex:=FS.lb.Items.IndexOf(s[sel.show].name); FS.lbClick(FEd) end;
      F1.ref_info_bClick(FEd)
    end
end;

procedure TFEd.FormShow(Sender: TObject);
begin
  Button2.Enabled:=true; Button2.Caption:='Save';
  if Sender=FS then showmessage('fs (error; see code)');
  //
  enm.Text:=s[sel.show].name;
  {}oldn:=enm.Text; newn:=oldn;
  els.Text:=inttostr(s[sel.show].last_episode div 1000);
  elep.Text:=inttostr(s[sel.show].last_episode mod 1000);
  {}oldep:=s[sel.show].last_episode; newep:=oldep;
  cha.Checked:=s[sel.show].active;
  eh.Text:=inttostr(s[sel.show].broadcast_hour);
  cb1.ItemIndex:=s[sel.show].dow; dow_changed:=false; cb2.ItemIndex:=s[sel.show].rating;
  if s[sel.show].last_episode_date=dateX then
    begin chd.Checked:=true; dtp.Date:=dateX end
  else
    begin chd.Checked:=false; dtp.Date:=s[sel.show].last_episode_date end
end;

end.
