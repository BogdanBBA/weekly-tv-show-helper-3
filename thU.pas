unit thU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TThForm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    thlb: TListBox;
    Label5: TLabel;
    Label6: TLabel;
    db: TButton;
    newb: TButton;
    editb: TButton;
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure thlbClick(Sender: TObject);
    procedure dbClick(Sender: TObject);
    procedure editbClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure newbClick(Sender: TObject);
    procedure thlbDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ThForm: TThForm;
  thl: array[1..200] of string;
  nth: word;

implementation

{$R *.dfm}

uses functii, data_types, wtvsh3_pas, thE;

function no_asterix(s: string): string;
begin result := stringreplace(s, '*', '', [rfReplaceAll]) end;

procedure save_theme_list;
var i: word;
begin
  log('Saving theme list...');
  assignfile(f, 'data\themes\list.thl'); rewrite(f); writeln(f, theme1_name);
  for i:=1 to nth do writeln(f, thl[i]); closefile(f)
end;

procedure read_theme_list;
begin
  log('Reading theme list...');
  assignfile(f, 'data\themes\list.thl'); reset(f); nth:=0; readln(f);
  while not eof(f) do begin inc(nth); readln(f, thl[nth]) end;
  closefile(f);
end;

procedure TThForm.Button1Click(Sender: TObject);
var i: word;
begin // HEY!!! better just scan for themes (don't have chef now, 20.04.12)
  log('Refreshing FTh...'); read_theme_list; thlb.Clear;
  for i:=1 to nth do if theme1_name=thl[i] then thlb.Items.Add(thl[i]+'*') else thlb.Items.Add(thl[i]);
  db.Enabled:=false; editb.Enabled:=false;
  thlb.ItemIndex:=thlb.Items.IndexOf(theme1_name+'*'); thlbClick(Button1)
end;

procedure TThForm.Button2Click(Sender: TObject);
begin
  ThForm.Close
end;

procedure TThForm.dbClick(Sender: TObject);
var phz, dlt, x: string; tl: TStringList; i: word;
begin
  dlt:=stringreplace(thlb.Items[thlb.ItemIndex], '*', '', [rfReplaceall]); if dlt='default' then exit; log('Deleting theme "'+dlt+'"...');
  if MessageDlg('Are you sure you want to delete the theme "'+dlt+'"?'+dnl+'This action can not be undone.', mtConfirmation, [mbYes, mbCancel], 0)=mrYes then
    begin try
        phz:='laoding default theme';
      thlb.ItemIndex:=0; thlbClick(db);
        phz:='deleting old theme';
      if not deletefile('data\themes\'+dlt+'.th') then showmessage('Deleting of the file has failed. Do this manually (filename: "data\themes\'+dlt+'.th").');
        phz:='updating theme list (part1/2)(create a list of existing themes by names in list.thl)';
      tl:=TStringList.Create; assignfile(f, 'data\themes\list.thl'); reset(f); readln(f);
      while not eof(f) do begin readln(f, x); if fileexists('data\themes\'+x+'.th') then tl.Add(x) end; closefile(f);
        phz:='updating theme list (part 2/2)(rewriting the theme list file)';
      rewrite(f); writeln(f, 'default'); for i:=0 to tl.Count-1 do writeln(f, tl[i]); closefile(f);
      log('Delete successfull'); showmessage('Success!'); Button1Click(db)
    except on E:Exception do showmessage('A weird error occured while deleting theme (phase="'+phz+'").'+dnl+E.ClassName+' error raised, with message : '+E.Message) end end
end;

procedure TThForm.editbClick(Sender: TObject);
begin
  ThEd.Show
end;

procedure TThForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if ThEd.Showing then ThEd.Close
end;

procedure TThForm.FormShow(Sender: TObject);
begin
  Button1Click(ThForm)
end;

procedure TThForm.newbClick(Sender: TObject);
var x: string;
begin
  if InputQuery('Create a new theme', 'Type in the name for the new theme:', x) then
    begin
      log('Creating new theme based on default.th...'); inc(nth); thl[nth]:=x;
      load_theme('default', theme1); save_theme(x, theme1);
      theme1_name:=x; save_theme_list;
      Button1Click(newb)
    end;
end;

procedure TThForm.thlbClick(Sender: TObject);
var i: word;
begin
  try
    theme1_name:=no_asterix(thlb.Items[thlb.ItemIndex]);
    editb.Enabled:=theme1_name<>'default'; db.Enabled:=editb.Enabled; save_theme_list;
    //
    log('Testing theme "'+theme1_name+'"...');
    load_theme(theme1_name, theme1); F1.Applytheme1Click(ThForm);
    //move * without refresh
    for i:=0 to thlb.Items.Count-1 do
      if no_asterix(thlb.Items[i])=theme1_name then thlb.Items[i]:=theme1_name+'*'
      else thlb.Items[i]:=no_asterix(thlb.Items[i])
  except on E:Exception do showmessage('A weird error occured while testing the theme.'+dnl+E.ClassName+' error raised, with message : '+E.Message) end
end;

procedure TThForm.thlbDblClick(Sender: TObject);
begin
  //thlbClick(thlb);
 if editB.Enabled then editbClick(thlb)
end;

end.
