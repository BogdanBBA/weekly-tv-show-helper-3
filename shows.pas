unit shows;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, StdCtrls, ExtCtrls, URLMon, JPEG, ShellAPI;

type
  TFS = class(TForm)
    lb: TListBox;
    Image1: TImage;
    PopupMenu1: TPopupMenu;
    ref_ed_b: TButton;
    Showeverything1: TMenuItem;
    Showonlyactiveshows1: TMenuItem;
    Showonlyshowswithmissinginfoorphoto1: TMenuItem;
    Showonlyinactiveshows1: TMenuItem;
    Label2: TLabel;
    Panel1: TPanel;
    filter_b: TButton;
    add_b: TButton;
    edit_b: TButton;
    Label1: TLabel;
    e1: TEdit;
    del_b: TButton;
    close_b: TButton;
    PopupMenuI: TPopupMenu;
    Showphoto1: TMenuItem;
    Checkphotosize1: TMenuItem;
    Image2: TImage;
    Openfullsizedphoto1: TMenuItem;
    Searchforimagesonline1: TMenuItem;
    procedure ref_ed_bClick(Sender: TObject);
    procedure Showeverything1Click(Sender: TObject);
    procedure Showonlyactiveshows1Click(Sender: TObject);
    procedure Showonlyshowswithmissinginfoorphoto1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Showonlyinactiveshows1Click(Sender: TObject);
    procedure lbClick(Sender: TObject);
    procedure close_bClick(Sender: TObject);
    procedure filter_bClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Panel1Resize(Sender: TObject);
    procedure edit_bClick(Sender: TObject);
    procedure add_bClick(Sender: TObject);
    procedure del_bClick(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Showphoto1Click(Sender: TObject);
    procedure Checkphotosize1Click(Sender: TObject);
    procedure Openfullsizedphoto1Click(Sender: TObject);
    procedure Searchforimagesonline1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FS: TFS;
  sl: TStringList;

implementation

{$R *.dfm}

uses data_types, functii, wtvsh3_pas, show_editor;

procedure TFS.add_bClick(Sender: TObject);
var x: string;
begin
  try
  if InputQuery('Add a new show', 'Type in the name of the show:', x) then
    begin
      log('Adding new show "'+x+'"...');
      inc(ns); setlength(s, ns+1); save_data; sel.show:=ns; sel.day:=0;
      s[ns].name:=x; s[ns].dow:=0; s[ns].broadcast_hour:=0; s[ns].rating:=0; s[ns].last_episode:=0; s[ns].last_episode_date:=dateX; s[ns].active:=true;
      FEd.Show
    end
  except on E:Exception do showmessage(e.ClassName+' raised an error with the message "'+e.Message+'". That''s not right!') end
end;

procedure TFS.close_bClick(Sender: TObject);
begin
  FS.Close
end;

procedure TFS.del_bClick(Sender: TObject);
var x, i: word; aux: TShow;
begin
  for x:=1 to ns do if s[x].name = lb.Items[lb.ItemIndex] then break;
  if not (x in [1..ns]) then begin showmessage('Select a show first!'); exit end;
  if MessageDlg('Are you sure you want to delete the show "'+s[x].name+'"?'+dnl+'This action can not be reversed.', mtWarning, mbYesNo, 0)=mrYes then
    begin
      for i:=x to ns-1 do s[i]:=s[i+1]; setlength(s, ns); inc(ns, -1); save_data;
      ref_ed_bClick(del_b)
    end
end;

procedure TFS.edit_bClick(Sender: TObject);
var x: word;
begin
  try
  try x:=show_pos_in_s(lb.Items[lb.ItemIndex]) except begin showmessage('A small error occured while searching for the show you want in the show list (I can''t find it). Filter the list to show everything (if this was what you wanted) and try again. '+'Will not open editor if it''s not already open. (edit_bClick: show_pos_in_s(lb.Items[lb.ItemIndex]))'); exit end end;
  if x=0 then begin showmessage('X=0 (WTF!?!). Will not open show editor.'); exit end;
  sel.show:=x; sel.day:=s[sel.show].dow;
  if not FEd.Showing then FEd.Show else FEd.OnShow(Sender)
  except on E:Exception do showmessage(e.ClassName+' raised an error with the message "'+e.Message+'". That''s not right!') end
end;

procedure TFS.filter_bClick(Sender: TObject);
var p: TPoint;
begin getcursorpos(p); Popupmenu1.Popup(p.X, p.Y) end;

procedure TFS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  sl.Free; F1.ref_info_bClick(FS)
end;

procedure TFS.FormResize(Sender: TObject);
begin
  Image1.Width:=FS.Width div 2-18; Image1.Height:=FS.Height-Panel1.Height-60;
  lb.Left:=Image1.Left+Image1.Width+6; lb.Width:=Image1.Width; lb.Height:=FS.Height-85;
  Panel1.Top:=Image1.Top+Image1.Height+6; Panel1.Width:=Image1.Width; Label2.Left:=lb.Left;
end;

procedure TFS.FormShow(Sender: TObject);
begin
  sl:=TStringList.Create; sl.Clear; sl.Duplicates:=dupIgnore; sl.CaseSensitive:=true; sl.Sorted:=true;
  e1.Text:=''; Showeverything1Click(FS);
  lb.ItemIndex:=lb.Items.IndexOf(s[sel.show].name); lbClick(FS)
end;

procedure TFS.Image1Click(Sender: TObject);
var p: TPoint;
begin
  try
  if lb.ItemIndex=-1 then begin showmessage('A small error occured while loading photo. The show you want doesn''t appear in the list. Filter the list to show everything (if this was what you wanted) and try again. (Image1Click)'); exit end;
  if fileexists('data\photos\'+lb.Items[lb.ItemIndex]+'.jpg') then
    begin Showphoto1.Caption:='Change photo for "'+lb.Items[lb.ItemIndex]+'"'; Checkphotosize1.Enabled:=true; Openfullsizedphoto1.Enabled:=true end
  else begin Showphoto1.Caption:='Select a show photo for "'+lb.Items[lb.ItemIndex]+'"'; Checkphotosize1.Enabled:=false; Openfullsizedphoto1.Enabled:=false end;
  if Sender=F1.imag then begin p.X:=FS.Left+image1.Left+image1.Width div 2; p.Y:=FS.Top+image1.Top+image1.Height div 2 end else getcursorpos(p);
  PopupMenuI.Popup(p.X, p.Y)
  except on E:Exception do showmessage(e.ClassName+' raised an error with the message "'+e.Message+'".'+dnl+'That''s not right! Maybe the show doen''t currently appear in the list. Try again.') end
end;

procedure TFS.lbClick(Sender: TObject);
begin
  try
  if lb.ItemIndex=-1 then begin showmessage('A small error occured while searching for the show you want in the show list (I can''t find it). Filter the list to show everything (if this was what you wanted) and try again. (lbClick)'); exit end;
  Label2.Caption:='Show list ('+inttostr(sl.Count)+'/'+inttostr(ns)+') : '+inttostr(lb.ItemIndex+1);
  if lb.Items.Count=0 then begin del_b.Enabled:=false; exit end;
  del_b.Enabled:=ns>1;
  if fileexists('data\photos\'+lb.Items[lb.ItemIndex]+'.jpg') then Image1.Picture.LoadFromFile('data\photos\'+lb.Items[lb.ItemIndex]+'.jpg')
  else Image1.Picture.LoadFromFile('data\logo2.png')
  except on E:Exception do showmessage(e.ClassName+' raised an error with the message "'+e.Message+'".'+dnl+'That''s not right! Maybe the show doen''t currently appear in the list. Filter them and try again.') end
end;

procedure TFS.Openfullsizedphoto1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('data\photos\'+lb.Items[lb.ItemIndex]+'.jpg'), nil, nil, SW_SHOW) end;

procedure TFS.Panel1Resize(Sender: TObject);
begin
  filter_b.Width:=Panel1.Width div 2-18; add_b.Width:=filter_b.Width; del_b.Width:=add_b.Width;
  edit_b.Width:=add_b.Width; close_b.Width:=add_b.Width; e1.Width:=add_b.Width;
  Label1.Left:=filter_b.Left+filter_b.Width+6; e1.Left:=label1.Left;
  del_b.Left:=e1.Left; close_b.Left:=e1.Left
end;

procedure TFS.ref_ed_bClick(Sender: TObject);
var i: word; ok: boolean;
begin
  try
  log('Refreshing editor show list...'); sl.Clear;
  for i:=1 to ns do
    begin
      if e1.Text='' then ok:=true else ok:=pos(ansiuppercase(e1.Text), ansiuppercase(s[i].name))<>0;
      if ok then
        begin
          ok:=false;
          if Showeverything1.Checked then ok:=true
          else if Showonlyactiveshows1.Checked then ok:=s[i].active
          else if Showonlyinactiveshows1.Checked then ok:=not s[i].active
          else if Showonlyshowswithmissinginfoorphoto1.Checked then
            ok:=((s[i].dow=0) or (s[i].broadcast_hour=0) or (s[i].rating=0) or (s[i].last_episode=0) or (not fileexists('data\photos\'+s[i].name+'.jpg')))
          else showmessage('nothing is checked (WTF!?!)');
          if ok then sl.Add(s[i].name)
        end
    end;
  lb.Clear; for i:=1 to sl.Count do lb.Items.Add(sl[i-1]);
  lb.ItemIndex:=min(1, lb.Items.Count)-1; lbClick(ref_ed_b)
  except on E:Exception do showmessage(e.ClassName+' raised an error with the message "'+e.Message+'" while refreshing editor show list.') end
end;

procedure TFS.Searchforimagesonline1Click(Sender: TObject);
begin Shellexecute(Handle, 'open', PChar('http://www.google.ro/search?gcx=w&q=hello&um=1&ie=UTF-8&hl=en&tbm=isch&source=og&sa=N&tab=wi&biw=1280&bih=877#q='+web_str(lb.Items[lb.ItemIndex])+'&um=1&hl=en&tbm=isch&source=lnt&tbs=isz:m&sa=X&ei=Wy1ET-2fNMOg4gTVpNQ8&ved=0CAoQpwUoAQ&fp=1&biw=1364&bih=683&bav=on.2,or.r_gc.r_pw.r_cp.r_qf.,cf.osb&cad=b'), nil, nil, SW_SHOW) end;

procedure TFS.Showeverything1Click(Sender: TObject);
begin Showeverything1.Checked:=true; Showonlyinactiveshows1.Checked:=false; Showonlyactiveshows1.Checked:=false; Showonlyshowswithmissinginfoorphoto1.Checked:=false; ref_ed_bClick(Showeverything1) end;

procedure TFS.Showonlyactiveshows1Click(Sender: TObject);
begin Showeverything1.Checked:=false; Showonlyinactiveshows1.Checked:=false; Showonlyactiveshows1.Checked:=true; Showonlyshowswithmissinginfoorphoto1.Checked:=false; ref_ed_bClick(Showonlyactiveshows1) end;

procedure TFS.Showonlyinactiveshows1Click(Sender: TObject);
begin Showeverything1.Checked:=false; Showonlyinactiveshows1.Checked:=true; Showonlyactiveshows1.Checked:=false; Showonlyshowswithmissinginfoorphoto1.Checked:=false; ref_ed_bClick(Showonlyinactiveshows1) end;

procedure TFS.Showonlyshowswithmissinginfoorphoto1Click(Sender: TObject);
begin Showeverything1.Checked:=false; Showonlyinactiveshows1.Checked:=false; Showonlyactiveshows1.Checked:=false; Showonlyshowswithmissinginfoorphoto1.Checked:=true; ref_ed_bClick(Showonlyshowswithmissinginfoorphoto1) end;

procedure TFS.Showphoto1Click(Sender: TObject);
var ok: boolean; p: string; op: TOpenDialog;
begin
  if pos('Change', Showphoto1.Caption)=0 then ok:=true
  else ok:=MessageDlg('Are you sure you want to change the already existing photo? It will be overwritten.', mtConfirmation, mbYesNo, 0)=mrYes;
  if ok then
    begin
      ok:=false;
      op:=TOpenDialog.Create(Self); op.Title:='Select new show photo (browse or type in a http:// address)';
      op.Filter:='*.jpg'; op.Options:=[ofFileMustExist]; op.InitialDir:=GetDesktopFolder;
      if op.Execute then
        begin
          p:=op.Filename;
          if pos('HTTP://', ansiuppercase(p))<>0 then ok:=download(p, 'data\photos\'+lb.Items[lb.ItemIndex]+'.jpg')
          else ok:=copyfile(pchar(p), pchar('data\photos\'+lb.Items[lb.ItemIndex]+'.jpg'), false);
          if ok then showmessage('Success!') else showmessage('Copying the photo has failed. Try again or do it manually.')
        end;
      op.Free;
      if ok then
        begin lbClick(Showphoto1) end
    end
end;

function jpeg_resize(file_name_and_path: string): boolean;
var bmp: TBitmap; jpg: TJpegImage; scale: Double;
begin
  try
  jpg := TJpegImage.Create;
  try
    jpg.Loadfromfile(file_name_and_path);
    if jpg.Height > jpg.Width then scale := screen.Height*0.25 / jpg.Height
    else scale := (screen.Width/3) / jpg.Width;
    bmp := TBitmap.Create;
    try
      bmp.Width := Round(jpg.Width * scale);
      bmp.Height:= Round(jpg.Height * scale);
      bmp.Canvas.StretchDraw(bmp.Canvas.Cliprect, jpg);
      //FS.Canvas.Draw(100, 10, bmp);
      jpg.Assign(bmp);
      jpg.SaveToFile(file_name_and_path);
    finally bmp.free end
  finally jpg.free end;
  result:=true
  except result:=false end
end;

procedure TFS.Checkphotosize1Click(Sender: TObject);
var w, h: word; sz: int64; ok: boolean; fn, xs: string; sr: TSearchRec; dt: TMsgDlgType; dbt: TMsgDlgButtons;
begin
  fn:='data\photos\'+lb.Items[lb.ItemIndex]+'.jpg'; findfirst(fn, 0, sr); sz:=sr.Size;
  Image2.Picture.LoadFromFile(fn); w:=Image2.Width; h:=Image2.Height; Image2.Picture:=nil;

  ok := (h<=screen.Height*0.375) and (w<=screen.Width/3*1.5);

  if ok then begin dt:=mtInformation; dbt:=[mbOK] end else begin dt:=mtConfirmation; dbt:=mbYesNo end;
  xs:='The photo has the following properties:'+dnl+'-File size: '+tkb(sz)+nl+'-Picture size: '+inttostr(w)+' x '+inttostr(h)+dnl;

  if not ok then xs:=xs+'Would you like to resize it?'+dnl+'Note: A backup will be made along the file so that you can revert to the original picture.'
  else xs:=xs+'You shouldn''t resize it.';

  if MessageDlg(xs, dt, dbt, 0)=mrYes then
    begin
      if not copyfile(pchar(fn), pchar('data\photos\larger_'+lb.Items[lb.ItemIndex]+'.jpg'), false) then
        begin showmessage('Backing up of the current photo has failed. Try again or resize manually.'+dnl+'It is a policy of this program to try and not lose data. If resizing fails, the photo will be lost. Since there is no backup, I will not proceed to resize.'); exit end;
      if not jpeg_resize(fn) then showmessage('Resizing has failed. Try again or do it manually.')
      else showmessage('Success!');
      if ok then lbClick(Checkphotosize1)
    end;
end;

end.
