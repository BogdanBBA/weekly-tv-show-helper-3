unit wtvsh3_pas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ShellAPI, Menus, ExtCtrls, jpeg, PngImage, ImgList,
  ComCtrls, MPlayer, ToolWin, DateUtils, MMSystem, XML.VerySimple;

const
  MCI_SETAUDIO = $0873; MCI_DGV_SETAUDIO_VOLUME = $4002; MCI_DGV_SETAUDIO_ITEM = $00800000; MCI_DGV_SETAUDIO_VALUE = $01000000; MCI_DGV_STATUS_VOLUME = $4019;
  ESNM: array[0..2] of byte=(4, 2, 1);

type MCI_DGV_SETAUDIO_PARMS = record
  dwCallback, dwItem, dwValue, dwOver: DWORD;
  lpstrAlgorithm, lpstrQuality: PChar;
end;

type MCI_STATUS_PARMS = record
  dwCallback, dwReturn, dwItem, dwTrack: DWORD;
end;

type
  TF1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    MainMenu1: TMainMenu;
    Application1: TMenuItem;
    Exit1: TMenuItem;
    Openlogfile1: TMenuItem;
    Whatsitfor1: TMenuItem;
    Howtouse1: TMenuItem;
    About1: TMenuItem;
    Edit1: TMenuItem;
    Export1: TMenuItem;
    AboutWTvSH31: TMenuItem;
    Appearance1: TMenuItem;
    hemes1: TMenuItem;
    Aboutthemes1: TMenuItem;
    Button1: TButton;
    imag: TImage;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    ListBox1: TListBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    prev_b: TButton;
    next_b: TButton;
    default_theme_save: TButton;
    Applytheme1: TMenuItem;
    refresh_b: TButton;
    ref_info_b: TButton;
    Displayeddata1: TMenuItem;
    Showallshows1: TMenuItem;
    Sort1: TMenuItem;
    sortM1: TMenuItem;
    sortM2: TMenuItem;
    sortM5: TMenuItem;
    sortM6: TMenuItem;
    Filter1: TMenuItem;
    Refresh1: TMenuItem;
    Showinfoforrandomshow1: TMenuItem;
    sortM3: TMenuItem;
    Weblinks1: TMenuItem;
    toWTvSH2format1: TMenuItem;
    toTXT1: TMenuItem;
    toHTML1: TMenuItem;
    Datastats1: TMenuItem;
    Ohand1: TMenuItem;
    PopupMenu1: TPopupMenu;
    Edit2: TMenuItem;
    SearchonEpguidescom1: TMenuItem;
    SearchonTorrentzcomnextep1: TMenuItem;
    Open1: TMenuItem;
    WTvSH3folder1: TMenuItem;
    Photosfolder1: TMenuItem;
    Datafolder1: TMenuItem;
    PopupMenu2: TPopupMenu;
    GoogleSearch1: TMenuItem;
    GoogleImages1: TMenuItem;
    IMDb1: TMenuItem;
    VGuide1: TMenuItem;
    Wikipedia1: TMenuItem;
    Wikipediaeplist1: TMenuItem;
    Epguidescom1: TMenuItem;
    orrentzcom1: TMenuItem;
    orrentzcomnextep1: TMenuItem;
    hePirateBay1: TMenuItem;
    hePirateBaynextep1: TMenuItem;
    SearchonEpguidescom2: TMenuItem;
    VGuide2: TMenuItem;
    Wikipediaeplist2: TMenuItem;
    srcE: TEdit;
    Timer1: TTimer;
    Backup1: TMenuItem;
    Music1: TMenuItem;
    Aboutmusic1: TMenuItem;
    Playonstartup1: TMenuItem;
    Playonshowclick1: TMenuItem;
    Playnow1: TMenuItem;
    Stopplaying1: TMenuItem;
    Selectthemesongforshow1: TMenuItem;
    mpl: TMediaPlayer;
    play_b: TButton;
    play_opt_b: TButton;
    PopupMenu3: TPopupMenu;
    Stopplaying2: TMenuItem;
    Selectathemesong1: TMenuItem;
    Playonstartup2: TMenuItem;
    Playonshowclick2: TMenuItem;
    Shortcutkeys1: TMenuItem;
    Youtube1: TMenuItem;
    Youtubethemesong1: TMenuItem;
    SearchforthemesongonYoutube1: TMenuItem;
    ogglefullscreen1: TMenuItem;
    AlwaysSeeWeekMonSun1: TMenuItem;
    Label11: TLabel;
    ImageList1: TImageList;
    pbTS: TProgressBar;
    Timer2: TTimer;
    Musicfolder1: TMenuItem;
    Searchonwikipediaeplist3: TMenuItem;
    PopupMenu4: TPopupMenu;
    Optionsforthisimage1: TMenuItem;
    Managephotosandmusic1: TMenuItem;
    bgImg: TImage;
    TimerFCap: TTimer;
    Openfullsizedimage1: TMenuItem;
    orrentzcomnextep2: TMenuItem;
    SearchonTorrentzcomnextep2: TMenuItem;
    IncreaseLWEandsave1: TMenuItem;
    Label12: TLabel;
    IncreaseLWEby2andsave1: TMenuItem;
    sortM4: TMenuItem;
    volTB: TTrackBar;
    orrentzcomnextand2ep1: TMenuItem;
    EpisodeX1: TMenuItem;
    Isthereanewepisode1: TMenuItem;
    UpdateLWEdata1: TMenuItem;
    Showdatafile1: TMenuItem;
    Help1: TMenuItem;
    PopupMenu5: TPopupMenu;
    TrayIcon1: TTrayIcon;
    TrayIconPopupMenu: TPopupMenu;
    Showhideapplication1: TMenuItem;
    Exit2: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure mplNotifyProc(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure Openlogfile1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure Whatsitfor1Click(Sender: TObject);
    procedure Aboutthemes1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure default_theme_saveClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Applytheme1Click(Sender: TObject);
    procedure refresh_bClick(Sender: TObject);
    procedure hemes1Click(Sender: TObject);
    procedure prev_bClick(Sender: TObject);
    procedure next_bClick(Sender: TObject);
    procedure lb_click(Sender: TObject);
    procedure lb_dbl_click(Sender: TObject);
    procedure ref_info_bClick(Sender: TObject);
    procedure starClick(Sender: TObject);
    procedure info_labels_size_check(Sender: TObject);
    procedure Edit1Click(Sender: TObject);
    procedure Showallshows1Click(Sender: TObject);
    procedure Showinfoforrandomshow1Click(Sender: TObject);
    procedure Label5DblClick(Sender: TObject);
    procedure Wikipedialistofepisodes1Click(Sender: TObject);
    procedure Wikipedia1Click(Sender: TObject);
    procedure Epguidecom1Click(Sender: TObject);
    procedure GoogleSearch1Click(Sender: TObject);
    procedure GoogleImages1Click(Sender: TObject);
    procedure IMDb1Click(Sender: TObject);
    procedure VGuide1Click(Sender: TObject);
    procedure orrentzcom1Click(Sender: TObject);
    procedure hePirateBay1Click(Sender: TObject);
    procedure Datastats1Click(Sender: TObject);
    procedure Howtouse1Click(Sender: TObject);
    procedure Ohand1Click(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure imagDblClick(Sender: TObject);
    procedure toTXT1Click(Sender: TObject);
    procedure orrentzcomnextep1Click(Sender: TObject);
    procedure hePirateBaynextep1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure WTvSH3folder1Click(Sender: TObject);
    procedure Datafolder1Click(Sender: TObject);
    procedure Photosfolder1Click(Sender: TObject);
    procedure Weblinks1Click(Sender: TObject);
    procedure srcEChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Filter1Click(Sender: TObject);
    procedure Backup1Click(Sender: TObject);
    procedure Aboutmusic1Click(Sender: TObject);
    procedure play_bClick(Sender: TObject);
    procedure Stopplaying1Click(Sender: TObject);
    procedure Selectthemesongforshow1Click(Sender: TObject);
    procedure PopupMenu3Popup(Sender: TObject);
    procedure play_opt_bClick(Sender: TObject);
    procedure Playonstartup1Click(Sender: TObject);
    procedure Playonshowclick1Click(Sender: TObject);
    procedure Shortcutkeys1Click(Sender: TObject);
    procedure Youtube1Click(Sender: TObject);
    procedure Youtubethemesong1Click(Sender: TObject);
    procedure ogglefullscreen1Click(Sender: TObject);
    procedure AlwaysSeeWeekMonSun1Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Musicfolder1Click(Sender: TObject);
    procedure lb_context_popup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure Managephotosandmusic1Click(Sender: TObject);
    procedure TimerFCapTimer(Sender: TObject);
    procedure Openfullsizedimage1Click(Sender: TObject);
    procedure PopupMenu4Popup(Sender: TObject);
    procedure orrentzcomnextep2Click(Sender: TObject);
    procedure ListBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncreaseLWEandsave1Click(Sender: TObject);
    procedure IncreaseLWEby2andsave1Click(Sender: TObject);
    procedure sortM1Click(Sender: TObject);
    procedure volTBChange(Sender: TObject);
    procedure orrentzcomnextand2ep1Click(Sender: TObject);
    procedure Isthereanewepisode1Click(Sender: TObject);
    procedure UpdateLWEdata1Click(Sender: TObject);
    procedure Showdatafile1Click(Sender: TObject);
    procedure Help1Click(Sender: TObject);
    procedure Showhideapplication1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F1: TF1;
  lb: array[0..7] of TListBox;
  lday: array[1..7] of TLabel;
  img: array[1..10] of TImage;

function lb_show_format(i: word): string;
function dow_corr(x: byte): byte;

implementation

uses data_types, functii, thU, show_editor, shows, stats, about, data_manager, Help2;

{
    Temporary changes made May 14th, 2013 for ICDD SIBIU 2013
      - hidden About1 button (logo&info) / bgImg disabled
      - hidden Managephotosandmusic1
      - hidden several buttons in AboutWTvSH31 after creating help
      - hidden links to torrent sites
      - hidden some buttons in PopupMenu1
    Partially (what was convenient) reverted May 25th, 2013
}

{$R *.dfm}

procedure MPSetVolume(MP: TMediaPlayer; Volume: Integer); //Set Volume, range 0 - 1000
var p: MCI_DGV_SETAUDIO_PARMS;
begin
  p.dwCallback := 0;
  p.dwItem := MCI_DGV_SETAUDIO_VOLUME;
  p.dwValue := Volume;
  p.dwOver := 0;
  p.lpstrAlgorithm := nil;
  p.lpstrQuality := nil;
  mciSendCommand(MP.DeviceID, MCI_SETAUDIO, MCI_DGV_SETAUDIO_VALUE or MCI_DGV_SETAUDIO_ITEM, Cardinal(@p))
end;

function MPGetVolume(MP: TMediaPlayer): Integer; //Get Volume, range 0 - 1000
var p: MCI_STATUS_PARMS;
begin
  p.dwCallback := 0;
  p.dwItem := MCI_DGV_STATUS_VOLUME;
  mciSendCommand(MP.DeviceID, MCI_STATUS, MCI_STATUS_ITEM, Cardinal(@p)) ;
  Result := p.dwReturn
end;

function lb_show_format(i: word): string;
begin
  if s[i].last_episode=0 then result := s[i].name
  else result := '['+FormatLastEpisode2(s[i].last_episode)+'] '+s[i].name
end;

function dow_corr(x: byte): byte;
var r: byte;
begin
  if F1.AlwaysSeeWeekMonSun1.Checked then r:=x else
  begin
    case x of
    1: begin case curr_dow of 1: r:=4; 2: r:=3; 3: r:=2; 4: r:=1; 5: r:=7; 6: r:=6; 7: r:=5; end end;
    2: begin case curr_dow of 1: r:=5; 2: r:=4; 3: r:=3; 4: r:=2; 5: r:=1; 6: r:=7; 7: r:=6; end end;
    3: begin case curr_dow of 1: r:=6; 2: r:=5; 3: r:=4; 4: r:=3; 5: r:=2; 6: r:=1; 7: r:=7; end end;
    4: begin case curr_dow of 1: r:=7; 2: r:=6; 3: r:=5; 4: r:=4; 5: r:=3; 6: r:=2; 7: r:=1; end end;
    5: begin case curr_dow of 1: r:=1; 2: r:=7; 3: r:=6; 4: r:=5; 5: r:=4; 6: r:=3; 7: r:=2; end end;
    6: begin case curr_dow of 1: r:=2; 2: r:=1; 3: r:=7; 4: r:=6; 5: r:=5; 6: r:=4; 7: r:=3; end end;
    7: begin case curr_dow of 1: r:=3; 2: r:=2; 3: r:=1; 4: r:=7; 5: r:=6; 6: r:=5; 7: r:=4; end end;
    else r:=0 end;
  end;
  result := r
end;

procedure TF1.About1Click(Sender: TObject);
begin FAb.Show end;

procedure TF1.Aboutmusic1Click(Sender: TObject);
begin showmessage('You have the ability to select an .mp3 file from your computer as the theme song for a particular show. This, when prompted (automatically, if enabled), will play, so that the atmosphere of the show is more prominent!'+dnl+'Your play settings (on startup, on show refresh) will be saved.'+dnl+'Note: although the Youtube search uses the words "theme song", you may only find the theme as "theme", "opening", "intro", so play around with these terms until you find what you want.') end;

procedure TF1.Aboutthemes1Click(Sender: TObject);
begin showmessage('Themes allow you to personalize the look of Weekly TV Shows Helper easily and intuitively!'+dnl+'You can modify the color of the form and listboxes, and every font on the main window! Edit existing themes (except the default one) or create new ones! Simple and fun!'+dnl+'PS: as of version 3.0, changing the window color to other than the original blue would require to change the logo and star background color as only .JPG images are supported for now. Will improve this soon!') end;

procedure TF1.Howtouse1Click(Sender: TObject);
begin showmessage('Weekly TV Show Helper 3 is designed to be as fast, simple and intuitive as possible!'+dnl+'One click of the mouse brings up info (that you''ve very easily added yourself) in an aesthetic manner in order for you to quickly see the information you want.'+dnl+'To edit shows, either double-click them from the show list (via "Show data" - "Edit" menu) or just double-click them in the main window. Data input boxes are labeled so that you always know what you''re typing. '+'Add (preferably all) the show''s information, then save, or click Close to cancel.'+dnl+'The same applies to the theme editor. Double-click on the theme list via the "Application" -> "Appearance" -> "Themes" '+'and then click directly on the labels to edit their font styles. The selected theme on closing the list window will be loaded next time you start the program.'+dnl+'Browse the menus and see what you can do. You won'' break anything! (well, hopefully :)) )'+
  dnl+'Things you should know:'+nl+'   - although I''ve tried to make this as reliable, easy, good-looking as fast as I can, this still is the work of, at most, a semi-professional. Things could go wrong in the program - '+'it won''t damage your computer, just the show data - but, in the end, you''re using it at your own responsability.'+nl+'   - you have two backup files for the shows: "old " and "oldest". But three saves with errors and you''ve lost the data. So back up manually now and then.'+nl+'   - don''t delete files in the data folder unless you know what you''re doing (for your own experience). At least make a backup copy before changing anything.'+nl+'   - there''s a limit of 200 themes, but no theoretical limit for shows; photos should only be .JPG; currently, only supports .JPG logo and stars.'+nl+'   - this is a work in progress; if there''s an error, try to go on, if not, restart; if it persists, or even better, '+
  'if you enjoy my work and have any suggestions that I might implement, contact me at bogdybba@gmail.com') end;

procedure TF1.imagDblClick(Sender: TObject);
begin
  if not FS.Showing then FS.Show;
  FS.lb.ItemIndex:=FS.lb.Items.IndexOf(s[sel.show].name);
  FS.lbClick(imag); FS.Image1Click(imag)
end;

procedure TF1.Ohand1Click(Sender: TObject);
begin showmessage('...it'' FrreeeeEeeEEeEeeEeee...!!!') end;

procedure check_theme_fonts(x: TTheme);
var fn, r: string; i: byte; missL: TStringList;
begin
  missL:=TStringList.Create;
  fn:=x.fonts.title; fn:=copy(fn, 1, pos('|',fn)-1); if Screen.Fonts.IndexOf(fn)=-1 then missL.Add(fn);
  fn:=x.fonts.subtitle_date; fn:=copy(fn, 1, pos('|',fn)-1); if Screen.Fonts.IndexOf(fn)=-1 then missL.Add(fn);
  fn:=x.fonts.subtitle_ep; fn:=copy(fn, 1, pos('|',fn)-1); if Screen.Fonts.IndexOf(fn)=-1 then missL.Add(fn);
  fn:=x.fonts.today_is; fn:=copy(fn, 1, pos('|',fn)-1); if Screen.Fonts.IndexOf(fn)=-1 then missL.Add(fn);
  fn:=x.fonts.today_label; fn:=copy(fn, 1, pos('|',fn)-1); if Screen.Fonts.IndexOf(fn)=-1 then missL.Add(fn);
  fn:=x.fonts.ldays; fn:=copy(fn, 1, pos('|',fn)-1); if Screen.Fonts.IndexOf(fn)=-1 then missL.Add(fn);
  fn:=x.fonts.lbs; fn:=copy(fn, 1, pos('|',fn)-1); if Screen.Fonts.IndexOf(fn)=-1 then missL.Add(fn);
  if missL.Count>0 then
    begin
      r:='You do not have all the required fonts for this theme. Below is a list of what you are missing.'+nl;
      for i:=0 to missL.Count-1 do r:=r+nl+'"'+missL[i]+'"';
      showmessage(r+dnl+'No error will occur, as Windows will probably substitute the fonts with something else.'+nl+'Install these ones and reapply the theme for full effect.')
    end;
  missL.Free
end;

procedure TF1.Applytheme1Click(Sender: TObject);
var i: word; x: TFont;
begin
  try
  with F1 do
    begin
      log('Applying current_theme...'); //x:=F1.Font;
      check_theme_fonts(theme1);
      F1.Color:=theme1.colors.form;
      StrToFont(theme1.fonts.title, x); Label5.Font:=x; log('Applied (successfully?) a TFont...');
      StrToFont(theme1.fonts.subtitle_date, x); Label6.Font:=x; Label12.Font:=x;
      StrToFont(theme1.fonts.subtitle_ep, x); Label7.Font:=x;
      StrToFont(theme1.fonts.today_is, x); Label9.Font:=x;
      StrToFont(theme1.fonts.today_label, x); Label10.Font:=x;
      for i:=1 to 7 do
        begin
          lb[i].Color:=theme1.colors.lbs;
          StrToFont(theme1.fonts.lbs, x); lb[i].Font:=x;
          StrToFont(theme1.fonts.ldays, x); lday[i].Font:=x
        end;
      if Sender=ThForm then F1.refresh_bClick(Applytheme1); F1.FormResize(Applytheme1);
      log('Applied theme "'+theme1_name+'" successfully!')
    end except on E:Exception do showmessage('An error has occured while trying to apply the theme.'+dnl+E.ClassName+' raised an error with the message "'+E.Message+'"') end
end;

procedure TF1.Backup1Click(Sender: TObject);
  {*}procedure CopyF(S, D: string);
  begin if not copyfile(pwidechar(S), pwidechar(D), false) then showmessage('Copying of file "'+S+'" has failed. Retry backup later. Continuing...') end;
var fn, x: string;
begin
  if MessageDlg('Would you like to backup the data?'+dnl+'This only includes "shows.xml" and the "themes" folder.', mtConfirmation, [mbYes, mbCancel], 0)=mrYes then
    try
      log('Trying to create backup...');
      //folders
      if not directoryexists('data\backups') then if not createdirectory('data\backups', nil) then begin showmessage('Failed to create folder "data\backups". Create it manually. Exiting...'); exit end;
      fn:=formatdatetime('"data\backups\backup "yyyy.mm.dd hh.nn', now); if not directoryexists(fn) then if not createdirectory(pwidechar(fn), nil) then begin showmessage('Failed to create backup folder "'+fn+'". Try again. Exiting...'); exit end;
      if not directoryexists(fn+'\themes') then if not createdirectory(pwidechar(fn+'\themes'), nil) then begin showmessage('Failed to create backup folder "'+fn+'\themes". Try again. Exiting...'); exit end;
      //files
      CopyF('data\shows.xml', fn+'\shows.xml'); CopyF('data\themes\list.thl', fn+'\themes\list.thl');
      assignfile(f, 'data\themes\list.thl'); reset(f); readln(f);
      while not eof(f) do begin readln(f, x); CopyF('data\themes\'+x+'.th', fn+'\themes\'+x+'.th') end; closefile(f);
      log('Backup created successfully!'); showmessage('Backup created successfully!'+dnl+'If you''d like to backup more program files (such as logos, or show images), why not archive the whole folder (or at least parts of it)?')

    except on E:Exception do showmessage('An error has occured while creating backup.'+dnl+E.ClassName+' raised an error with the message "'+E.Message+'"') end
end;

procedure TF1.AlwaysSeeWeekMonSun1Click(Sender: TObject);
begin
  AlwaysSeeWeekMonSun1.Checked:=not AlwaysSeeWeekMonSun1.Checked; Label10.Enabled:=not AlwaysSeeWeekMonSun1.Checked; prev_b.Enabled:=Label10.Enabled;
  next_b.Enabled:=prev_b.Enabled; sett.seeWeekMondaytoSunday:=AlwaysSeeWeekMonSun1.Checked; if Sender=Button1 then exit; save_settings; refresh_bClick(AlwaysSeeWeekMonSun1)
end;

procedure TF1.Datafolder1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('data'), nil, nil, SW_SHOW) end;

procedure TF1.Datastats1Click(Sender: TObject);
begin if FSt.Showing then FSt.OnShow(Datastats1) else FSt.Show end;

procedure TF1.default_theme_saveClick(Sender: TObject);
var x: TTheme;
begin
  try
  //
  x.colors.form:=F1.Color; x.colors.lbs:=ListBox1.Color;
  x.fonts.title:=FontToStr(Label5.Font); x.fonts.subtitle_date:=FontToStr(Label6.Font); x.fonts.subtitle_ep:=FontToStr(Label7.Font);
  x.fonts.today_is:=FontToStr(Label9.Font); x.fonts.today_label:=FontToStr(Label10.Font);
  x.fonts.ldays:=FontToStr(Label8.Font); x.fonts.lbs:=FontToStr(ListBox1.Font);
  //
  assignfile(fth, 'data\themes\default.th'); rewrite(fth); write(fth, x); closefile(fth); showmessage('Saved default theme successfully')
  except on E:Exception do showmessage('error: '+E.ClassName+' - "'+E.Message+'"') end
end;

procedure TF1.Edit1Click(Sender: TObject);
begin FS.Show end;

procedure TF1.Exit1Click(Sender: TObject);
begin
  try
  if FEd.Showing then FEd.Close;
  if FS.Showing then FS.Close;
  if ThForm.Showing then ThForm.Close;
  if FM.Showing then FM.Close;
  if FSt.Showing then FSt.Close;
  if FAb.Showing then FAb.Close;
  if FH.Showing then FH.Close;
  if mpl.Mode=mpPlaying then mpl.Close;
  Halt
  except on E:Exception do showmessage('An error has occured while closing (probably while closing other forms and media player).'+dnl+E.ClassName+' raised an error with the message "'+E.Message+'"') end
end;

procedure TF1.Filter1Click(Sender: TObject);
begin srcE.SetFocus; srcE.SelectAll end;

procedure TF1.FormCreate(Sender: TObject);
var i: word; mi: TMenuItem;
begin
  with FormatSettings do begin DecimalSeparator:='.'; ThousandSeparator:=','; DateSeparator:='/'; TimeSeparator:=':' end;
  assignfile(f, 'data\log.txt'); rewrite(f); closefile(f); log('Reset log file'); log('Creating window objects...');
  bgImg.SendToBack; bgImg.AutoSize:=true; if fileexists('data\bgImg.png') then bgImg.Picture.LoadFromFile('data\bgImg.png');
  TrayIcon1.Visible:=True; TrayIcon1.Animate:=True;
  mpl.TimeFormat:=tfMilliseconds;
  //
  for i:=1 to 7 do
    begin
      lday[i]:=TLabel.Create(Self); lday[i].Parent:=Self; lday[i].Caption:='lday'+inttostr(i);
        //lday[i].Font:=Label8.Font;
    end;
  //
  for i:=0 to 7 do
    begin
      lb[i]:=TListBox.Create(Self); lb[i].Parent:=Self;
      lb[i].OnClick:=lb_click; lb[i].OnDblClick:=lb_dbl_click; lb[i].OnMouseDown:=ListBox1MouseDown
    end;
  lb[0].Visible:=false;
  //
  for i:=1 to 10 do
    begin
      img[i]:=TImage.Create(Self); img[i].Parent:=Self; img[i].Center:=true; img[i].Proportional:=true; img[i].Stretch:=true;
      img[i].Picture.LoadFromFile('data\starG.png'); img[i].Cursor:=crHandPoint;
      img[i].OnClick:=starClick; img[i].PopupMenu:=PopupMenu5;
      img[i].Picture.RegisterFileFormat('.PNG', 'Portable Network Graphics', TPNGImage)
    end;
  //
  for i:=0 to 10 do
    begin
      mi:=TMenuItem.Create(Self); mi.ImageIndex:=219; mi.Caption:='Rate '+inttostr(i);
      mi.OnClick:=starClick; PopupMenu5.Items.Add(mi)
    end;
  //
  imag.Picture.LoadFromFile('data\logo2.png'); Label5.Caption:='---'; Label6.Caption:='---'; Label7.Caption:='---';
  mpl.Notify:=True; mpl.OnNotify:=mplNotifyProc;
  init_read_data;
end;

procedure TF1.mplNotifyProc(Sender: TObject);
begin
   with Sender as TMediaPlayer do
   begin
     case Mode of mpStopped: play_b.Enabled:=fileexists('data\music\'+s[sel.show].name+'.mp3') end;
     Notify := True //must set to true to enable next-time notification
   end
end;

procedure TF1.FormShow(Sender: TObject);
var x, nit: word; ok: boolean;
begin
  Applytheme1Click(F1); TimerFCapTimer(F1);
  sel.show:=0; sel.day:=0; log('Getting initial show (for the info to be shown); searching yesterday...'); ok:=false;
  nit:=0; repeat inc(nit); x:=random(ns)+1 until ((s[x].active) and (s[x].rating>6) and (s[x].last_episode_date<>dateX) and (s[x].dow=curr_dow-1)) or (nit=400); sel.show:=x; sel.day:=s[x].dow;
  if (sel.show=0) or (nit=400) then begin log('Nothing from yesterday fits the criteria; getting another show info...'); nit:=0; repeat inc(nit); x:=random(ns)+1 until ((s[x].active) and (s[x].rating>5)) or (nit=400); sel.show:=x; sel.day:=s[x].dow end else ok:=true;
  if sel.show=0 then log('Nothing fits the criteria. Maybe there aren''t any shows.') else log('Finally got s[x='+inttostr(x)+'] = "'+s[x].name+'"');
  sortM3.Checked:=true; refresh_bClick(F1); ref_info_bClick(Button1); if ok then begin Label5.Caption:='Yesterday: '+Label5.Caption; info_labels_size_check(F1) end; //must be Button1
  log('Successfully initialized program!')
end;

procedure TF1.hemes1Click(Sender: TObject);
begin ThForm.Show end;

procedure TF1.FormResize(Sender: TObject);
var i, w, h: word; //laux: ^TLabel;
begin
  w:=F1.Width; h:=F1.Height;
  //
  imag.Height:=round(h/4); imag.Width:=round(w/3); //showmessage('img size: '+inttostr(img.Width)+' x '+inttostr(img.Height)+', ratio '+formatfloat('0.000', img.Width/img.Height));
  //
  Label5.Left:=imag.Left+imag.Width+6; Label5.Font.Size:=round((imag.Height/3)/1.5);
  Label6.Left:=Label5.Left; Label6.Top:=Label5.Top+Label5.Height+6; Label6.Font.Size:=round((imag.Height/6)/1.5);
  Label7.Left:=Label5.Left; Label7.Font.Size:=round((imag.Height/4.82)/1.5); Label7.Top:=(imag.Top+imag.Height)-Label7.Height;
  //
  Label9.Top:=imag.Top+imag.Height+6; Label10.Top:=Label9.Top; Label10.Left:=Label9.Left+Label9.Width+6;
  prev_b.Left:=Label10.Left+Label10.Width+6; next_b.Left:=prev_b.Left+prev_b.Width+6;
  prev_b.Top:=Label10.Top-5; next_b.Top:=prev_b.Top;
  srcE.Left:=next_b.Left+next_b.Width+6; srcE.Top:=next_b.Top+5; srcE.Width:=w div 2 - srcE.Left;
  //
  img[1].Top:=Label6.Top+Label6.Height+6; img[1].Left:=Label6.Left; img[1].Height:=Label7.Top-Label6.Top-Label6.Height-6; img[1].Width:=img[1].Height;
  for i:=2 to 10 do
    begin
      img[i].Top:=img[1].Top; img[i].Left:=img[i-1].Left+img[i-1].Width+2;
      img[i].Width:=img[1].Width; img[i].Height:=img[1].Height
    end;
  //
  for i:=1 to 7 do lday[i].Top:=Label9.Top+Label9.Height+6;
  lb[1].Width:=round((w-48-15*integer((F1.BorderStyle=bsSizeable) or (F1.WindowState=wsNormal)))/7); for i:=2 to 7 do lb[i].Width:=lb[1].Width;
  lb[1].Left:=6; for i:=2 to 7 do lb[i].Left:=lb[i-1].Left+lb[i-1].Width+6;
  for i:=1 to 7 do
    begin lday[i].Left:=lb[i].Left; lb[i].Top:=lday[i].Top+lday[i].Height+6; lb[i].Height:=h-lb[i].Top-Button1.Height-32-40*integer((F1.BorderStyle=bsSizeable) or (F1.WindowState=wsNormal)) end;
  //
  play_opt_b.Top:=next_b.Top; play_opt_b.Left:=lb[7].Left+lb[7].Width-play_opt_b.Width;
  play_b.Top:=next_b.Top; play_b.Left:=lb[7].Left; play_b.Width:=lb[7].Width-play_opt_b.Width-3;
  Label12.Font.Size:=round((imag.Height/10)/1.5); Label12.Top:=Label6.Top+(Label6.Height-label12.Height)-2; Label12.Left:=lb[7].Left+lb[7].Width-Label12.Width;
  //
  bgImg.Left:=lb[1].Left; bgImg.Top:=lb[1].Top+lb[1].Height+2;
  Button1.Top:=h-Button1.Height-25-40*integer((F1.BorderStyle=bsSizeable) or (F1.WindowState=wsNormal)); Button1.Left:=lb[7].Left+(lb[7].width-Button1.Width);
  Button1.Width:=max(lb[7].width, 175); Label1.Top:=Button1.Top+(Button1.Height-Label1.Height)-2; Label1.Left:=Button1.Left-Label1.Width-6;
  pbTS.Left:=lb[1].Left; pbTS.Top:=Button1.Top+2; pbTS.Width:=w-(w-Button1.Left)-12;
  volTB.Left:=pbTS.Left-6; volTB.Top:=pbTS.Top+pbTS.Height+10; volTB.Width:=lb[1].Width*2+16;
  //
  F1.info_labels_size_check(F1)
end;

procedure TF1.info_labels_size_check(Sender: TObject);
var i: word;
begin
  for i:=5 to 7 do
    begin
      //laux:=@TLabel(FindComponent('Label'+inttostr(i)));
      while TLabel(FindComponent('Label'+inttostr(i))).Canvas.TextWidth(TLabel(FindComponent('Label'+inttostr(i))).Caption) > F1.Width-TLabel(FindComponent('Label'+inttostr(i))).Left-12 do
        TLabel(FindComponent('Label'+inttostr(i))).Font.Size:=TLabel(FindComponent('Label'+inttostr(i))).Font.Size-1
    end;
end;

procedure TF1.Isthereanewepisode1Click(Sender: TObject);
var i: longint; ad, df, xs: string; sl: TStringList; //xt: TXMLVerySimple;
    InetIsOffline: function(dwFlags: DWORD): BOOL; stdcall;
begin
  //check if connected to the internet
  if FuncAvail('URL.DLL', 'InetIsOffline', @InetIsOffline) then
    if InetIsOffLine(0) = true then begin MessageDlg('You don''t seem to be connected to tyhe internet. Sorry!', mtWarning, [mbOK], 0); exit end;
  //
  df:='data\temp.xml';
  if s[sel.show].custom_epg_address<>'' then ad:=s[sel.show].custom_epg_address
  else ad:='http://epguides.com/'+fileword(s[sel.show].name)+'/';
  //try and retry to get web file
  if not GetInetFile(ad, df) then
    begin
      if not InputQuery('EPG show address', 'Downloading the episode list from EPG failed.'+dnl+'Please type the page address so that I can try again.', ad) then exit;
      if not GetInetFile(ad, df) then begin MessageDlg('Webpage "'+ad+'" could not be downloaded to file "'+df+'". Sorry about that.', mtError, [mbOK], 0); exit end
      else s[sel.show].custom_epg_address:=ad
    end
  else s[sel.show].custom_epg_address:=ad;
  save_data;
  //cleanup
  try
    sl:=TStringList.Create; sl.LoadFromFile(df);
    if sl.IndexOf('<div id="eplist">')=-1 then Raise Exception.CreateFmt('Invalid code: ''%s''', ['no "<div id="eplist">" text found']);
    while pos('<div id="eplist">', sl[0])=0 do sl.Delete(0);
    if sl.IndexOf('</div>')=-1 then Raise Exception.CreateFmt('Invalid code: ''%s''', ['no "</div>" text found']);
    while sl.IndexOf('</div>')<sl.Count-1 do sl.Delete(sl.Count-1);
    for i:=0 to sl.Count-1 do begin xs:=sl[i]; clean_str(xs, true, true); sl[i]:=xs end;
    sl.SaveToFile(df);
    //not sure if XML is useful
    //xt:=TXMLVerySimple.Create; xt.LoadFromFile(df); xt.Free
    MessageDlg('Hi! For now, WTvSH3 is not smart enough to be able to decode the episode from the code source, but why don''t you read it and think about what could be done in the future?', mtWarning, [mbOK], 0);
    ShellExecute(Handle, 'open', PChar(df), nil, nil, SW_SHOW)
  except on E:Exception do
     MessageDlg('An error was encountered while extracting data from temporary file.'+dnl+E.ClassName+' - '+E.Message, mtError, [mbOK], 0)
  end;
end;

procedure TF1.Label10Click(Sender: TObject);
begin curr_dow:=dow; refresh_bClick(Label10) end;

procedure TF1.Label5DblClick(Sender: TObject);
begin
  if not FS.Showing then FS.Show;
  FS.lb.ItemIndex:=FS.lb.Items.IndexOf(s[sel.show].name); FS.lbClick(F1.Label5); FS.edit_bClick(F1.Label5)
end;

procedure TF1.sortM1Click(Sender: TObject);
var i: word;
begin
  for i:=1 to 6 do TMenuItem(FindComponent('sortM'+inttostr(i))).Checked := Sender = FindComponent('sortM'+inttostr(i));
  refresh_bclick(Sender)
end;

procedure TF1.srcEChange(Sender: TObject);
begin Timer1.Enabled:=false; Timer1.Enabled:=true end;

procedure TF1.starClick(Sender: TObject);
var x: word;
begin
  if sel.show=0 then begin showmessage('Select a show first!'); exit end;
  if Sender=PopupMenu5.Items[0] then x:=0
  else for x:=1 to 10 do if (Sender=img[x]) or (Sender=PopupMenu5.Items[x]) then break;
  log('Aplying rating '+inttostr(x)+' to show "'+s[sel.show].name+'"...');
  s[sel.show].rating:=x; save_data; ref_info_bClick(img[x])
end;

procedure TF1.Stopplaying1Click(Sender: TObject);
begin if mpl.Mode=mpPlaying then mpl.Stop; play_b.Enabled:=fileexists('data\music\'+s[sel.show].name+'.mp3') end;

procedure TF1.Timer1Timer(Sender: TObject);
begin Timer1.Enabled:=false; refresh_bClick(Timer1) end;

procedure TF1.Timer2Timer(Sender: TObject);
begin
  Timer2.Enabled:=mpl.Mode=mpPlaying; pbTS.Visible:=Timer2.Enabled; volTB.Visible:=pbTS.Visible; pbTS.Position:=round(mpl.Position/mpl.Length*1000);
  if not Timer2.Enabled then play_b.Enabled:=fileexists('data\music\'+s[sel.show].name+'.mp3')
end;

procedure TF1.TimerFCapTimer(Sender: TObject);
begin F1.Caption:='Weekly TV Show Helper 3'+formatdatetime('   -   mmmm d', now)+number_suffix(strtoint(formatdatetime('d', now)))+formatdatetime(', yyyy - hh:nn:ss', now) end;

procedure TF1.toTXT1Click(Sender: TObject);
var i: word; fn, x1, x2: string; xn: int64;
begin
  get_data_ready;
  fn:='data\text export '+formatdatetime('yyyy.mm.dd" at "hh.nn', now)+'.txt'; assignfile(f, fn); rewrite(f);
  x1:='====================';  x1:=x1+x1; x2:=x1+x1;
  write(f, x2+nl+'    TEXT EXPORT'+dnl+'    Date: '+formatdatetime('dddd, d mmmm yyyy, hh:nn', now)+nl+'    Number of shows: '+inttostr(ns)+nl);
  write(f, '    Active / inactive: '+inttostr(st.nas)+' / '+inttostr(st.nias)+nl+'    Over '+inttostr(st.nd)+' day(s)'+nl);
  write(f, '    Shows per day: '+formatfloat('"average "0.00" (range "', st.avgSpD)+inttostr(st.minSpD)+'-'+inttostr(st.maxSpD)+')'+nl);
  write(f, '    Broadcasting hour: average '+inttostr(trunc(st.avgBH))+':'+formatfloat('00" (range "', frac(st.avgBH)*60)+inttostr(st.minBH)+'-'+inttostr(st.maxBH)+')'+nl);
  write(f, '    Ratings: '+formatfloat('"average "0.00" (range "', st.avgR)+inttostr(st.minR)+'-'+inttostr(st.maxR)+')'+nl);
  write(f, '    Oldest last watched episode: '+formatdatetime('d mmmm yyyy" (', st.olwep)+st.olwepSTR+')"'+nl);
  write(f, '    Newest last watched episode: '+formatdatetime('d mmmm yyyy" (', st.nlwep)+st.nlwepSTR+')"'+nl);
  write(f, '    Average date of last watched episode: '+formatdatetime('d mmmm yyyy', st.avglwep)+nl);
  write(f, '    Photos (total size): '+inttostr(st.nph)+' (/'+inttostr(st.ns)+', '+tkb(st.tPhSz)+')'+nl);
  write(f, '    Photo sizes: average '+tkb(st.avgPhSz)+' (range '+tkb(st.minPhSz)+'-'+tkb(st.maxPhSz)+')'+nl);
  write(f, '    Theme songs (total size): '+inttostr(st.nTS)+' (/'+inttostr(st.ns)+', '+tkb(st.tTSS)+')'+nl);
  write(f, '    Theme song sizes: average '+tkb(st.avgTSS)+' (range '+tkb(st.minTSS)+'-'+tkb(st.maxTSS)+')'+nl);
  xn:=0; if st.nph+st.nTS<>0 then xn:=round((st.tPhSz+st.tTSS)/(st.nph+st.nTS));
  write(f, '    Total utility files size: '+tkb(st.tPhSz+st.tTSS)+' ('+inttostr(st.nph+st.nTS)+' files, avg. '+tkb(xn)+')'+nl);
  write(f, '    Memory used for one show: '+tkb(st.mem1S)+nl);
  write(f, '    Memory used for all data: '+tkb(st.memAllData)+nl);
  write(f, x2+dnl+x1+nl);
  for i:=1 to ns do
    begin
      write(f, '        '+inttostr(i)+'. '+s[i].name+nl+x1+nl); if s[i].active then write(f, '    Is being watched at this time'+nl) else write(f, '    Is inactive at this time'+nl);
      if s[i].dow<>0 then write(f, '    Broadcasts '+d[s[i].dow].name+'s at '+inttostr(s[i].broadcast_hour)+':00'+nl) else write(f, '    No one knows when it broadcasts'+nl);
      write(f, '    Last episode: ');
      if s[i].last_episode=0 then write(f, 'no one knows'+nl) else if s[i].last_episode_date=dateX then write(f, FormatLastEpisode(s[i].last_episode)+nl) else write(f, FormatLastEpisode(s[i].last_episode)+formatdatetime(' (d mmmm yyyy)', s[i].last_episode_date)+nl);
      if s[i].rating=0 then write(f, '    Has no rating') else write(f, '    Has a rating of '+inttostr(s[i].rating)+'/10');
      write(f, nl+x1+nl)
    end;
  closefile(f); ShellExecute(Handle, 'open', PChar(fn), nil, nil, SW_SHOW)
end;

procedure TF1.UpdateLWEdata1Click(Sender: TObject);
begin
  MessageDlg('This feature is not yet available.', mtInformation, [mbOK], 0)
end;

procedure TF1.Openfullsizedimage1Click(Sender: TObject);
var x: string;
begin x:='data\photos\'+s[sel.show].name+'.jpg'; ShellExecute(Handle, 'open', PChar(x), nil, nil, SW_SHOW) end;

procedure TF1.Openlogfile1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('data\log.txt'), nil, nil, SW_SHOW) end;

procedure TF1.prev_bClick(Sender: TObject);
begin curr_dow:=prevd(curr_dow); refresh_bClick(prev_b) end;

procedure TF1.next_bClick(Sender: TObject);
begin curr_dow:=nextd(curr_dow); refresh_bClick(next_b) end;

procedure TF1.ogglefullscreen1Click(Sender: TObject);
begin
  if F1.BorderStyle=bsSizeable then begin F1.BorderStyle:=bsNone; F1.WindowState:=wsMaximized end
  else begin F1.BorderStyle:=bsSizeable; F1.WindowState:=wsNormal end;
  F1.OnResize(ogglefullscreen1)
end;

procedure TF1.refresh_bClick(Sender: TObject);
var i, j, nsshown: word; ok: boolean;
begin
  get_data_ready; log('Refreshing...');
  Label10.Caption:=d[dow].name; nsshown:=0;
  //
  for i:=1 to 7 do
    if lday[dow_corr(i)].Canvas.TextWidth(d[i].name)>lb[i].Width then lday[dow_corr(i)].Caption:=d[i].name_short else lday[dow_corr(i)].Caption:=d[i].name;
  for i:=1 to 7 do lb[i].clear;
  for i:=1 to 7 do
    for j:=1 to d[i].shows.Count do
      begin
        ok:=false; if Showallshows1.Checked then ok:=true else ok:=s[show_pos_in_s(d[i].shows[j-1])].active;
        if ok then
          begin
            lb[dow_corr(s[show_pos_in_s(d[i].shows[j-1])].dow)].Items.Add(lb_show_format(show_pos_in_s(d[i].shows[j-1])));
            inc(nsshown)
          end
      end;
  log('Refreshed. Showing '+inttostr(nsshown)+' of '+inttostr(ns)+' shows...')
end;

procedure TF1.Weblinks1Click(Sender: TObject);
var p: TPoint;
begin getcursorpos(p); PopupMenu2.Popup(p.X, p.Y) end;

procedure TF1.Whatsitfor1Click(Sender: TObject);
begin showmessage('Weekly TV Show Helper does just what its name says. It helps you keep track of your weekly (or not exactly weekly) TV shows.'+dnl+'By inputting information such as broadcast day and hour, or the last episode watched number and date - with lightning speed - you can overview which shows air in what day, what you''ve watched last and enjoy doing all of this accounting!'+dnl+'You can add, edit, delete, filter and sort shows, quickly add and automatically resize photos for them, easily bring up only the shows you want and view statistical information about everything - only with this program!') end;

procedure TF1.VGuide1Click(Sender: TObject);
begin Shellexecute(Handle, 'open', PChar('http://www.tvguide.com/search/index.aspx?keyword='+web_str(s[sel.show].name)), nil, nil, SW_SHOW) end;

procedure TF1.volTBChange(Sender: TObject);
begin MPSetVolume(mpl, volTB.Position*10); mpl.Play end;

procedure TF1.orrentzcom1Click(Sender: TObject);
begin Shellexecute(Handle, 'open', PChar('http://torrentz.eu/search?q='+web_str(s[sel.show].name)), nil, nil, SW_SHOW) end;

procedure TF1.orrentzcomnextand2ep1Click(Sender: TObject);
begin orrentzcomnextep1Click(orrentzcomnextand2ep1); orrentzcomnextep2Click(orrentzcomnextand2ep1) end;

procedure TF1.orrentzcomnextep1Click(Sender: TObject);
begin if s[sel.show].last_episode=0 then showmessage('You should tell me what was the last episode you''ve seen first.') else Shellexecute(Handle, 'open', PChar('http://torrentz.eu/search?q='+web_str(s[sel.show].name)+formatfloat('"+S"00', s[sel.show].last_episode div 1000)+formatfloat('"E"00', s[sel.show].last_episode mod 1000+1)), nil, nil, SW_SHOW) end;

procedure TF1.orrentzcomnextep2Click(Sender: TObject);
begin if s[sel.show].last_episode=0 then showmessage('You should tell me what was the last episode you''ve seen first.') else Shellexecute(Handle, 'open', PChar('http://torrentz.eu/search?q='+web_str(s[sel.show].name)+formatfloat('"+S"00', s[sel.show].last_episode div 1000)+formatfloat('"E"00', s[sel.show].last_episode mod 1000+2)), nil, nil, SW_SHOW) end;

procedure TF1.GoogleImages1Click(Sender: TObject);
begin Shellexecute(Handle, 'open', PChar('http://www.google.ro/search?gcx=w&q=hello&um=1&ie=UTF-8&hl=en&tbm=isch&source=og&sa=N&tab=wi&biw=1280&bih=877#q='+web_str(s[sel.show].name)+'&um=1&hl=en&tbm=isch&source=lnt&tbs=isz:m&sa=X&ei=Wy1ET-2fNMOg4gTVpNQ8&ved=0CAoQpwUoAQ&fp=1&biw=1364&bih=683&bav=on.2,or.r_gc.r_pw.r_cp.r_qf.,cf.osb&cad=b'), nil, nil, SW_SHOW) {if modifying link, also change for FS.Searchforimagesonline1Click} end;

procedure TF1.GoogleSearch1Click(Sender: TObject);
begin Shellexecute(Handle, 'open', PChar('http://www.google.ro/search?sourceid=chrome&ie=UTF-8&q='+web_str(s[sel.show].name)), nil, nil, SW_SHOW) end;

procedure TF1.Help1Click(Sender: TObject);
begin FH.Show end;

procedure TF1.hePirateBay1Click(Sender: TObject);
begin Shellexecute(Handle, 'open', PChar('http://thepiratebay.se/search/'+web_str(s[sel.show].name)), nil, nil, SW_SHOW) end;

procedure TF1.hePirateBaynextep1Click(Sender: TObject);
begin if s[sel.show].last_episode=0 then showmessage('You should tell me what was the last episode you''ve seen first.') else Shellexecute(Handle, 'open', PChar('http://thepiratebay.se/search/'+web_str(s[sel.show].name)+formatfloat('" S"00', s[sel.show].last_episode div 1000)+formatfloat('"E"00', s[sel.show].last_episode mod 1000+1)), nil, nil, SW_SHOW) end;

procedure TF1.IMDb1Click(Sender: TObject);
begin Shellexecute(Handle, 'open', PChar('http://www.imdb.com/find?q='+web_str(s[sel.show].name)+'&s=all'), nil, nil, SW_SHOW) end;

procedure TF1.IncreaseLWEandsave1Click(Sender: TObject);
var x: word;
begin
  if s[sel.show].last_episode=0 then s[sel.show].last_episode:=1001 else inc(s[sel.show].last_episode);
  s[sel.show].last_episode_date:=incday(s[sel.show].last_episode_date, 7); s[sel.show].last_episode_modified:=now;
  for x:=1 to 7 do if lb[x].ItemIndex<>-1 then break; if lb[x].ItemIndex=-1 then exit;
  save_data; lb[x].Items[lb[x].ItemIndex]:=lb_show_format(sel.show); lb_click(lb[x]);
  log('Increased last watched episode for show "'+s[sel.show].name+'" to '+FormatLastEpisode(s[sel.show].last_episode)+' and saved data...')
end;

procedure TF1.IncreaseLWEby2andsave1Click(Sender: TObject);
begin IncreaseLWEandsave1Click(IncreaseLWEby2andsave1); IncreaseLWEandsave1Click(IncreaseLWEby2andsave1) end;

procedure TF1.Epguidecom1Click(Sender: TObject);
begin if s[sel.show].custom_epg_address<>'' then ShellExecute(Handle, 'open', PChar(s[sel.show].custom_epg_address), nil, nil, SW_SHOW) else Shellexecute(Handle, 'open', PChar('http://www.google.com/search?hl=en&q=allintitle%3A&q=site%3Aepguides.com&q='+web_str(s[sel.show].name)+'&btnG=Search'), nil, nil, SW_SHOW) end;

procedure TF1.Wikipedia1Click(Sender: TObject);
begin Shellexecute(Handle, 'open', PChar('http://en.wikipedia.org/w/index.php?title=Special:Search&search='+web_str(s[sel.show].name)), nil, nil, SW_SHOW) end;

procedure TF1.Wikipedialistofepisodes1Click(Sender: TObject);
begin Shellexecute(Handle, 'open', PChar('http://en.wikipedia.org/w/index.php?title=Special:Search&search=List+of+'+web_str(s[sel.show].name)+'+episodes'), nil, nil, SW_SHOW) end;

procedure TF1.WTvSH3folder1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar(getcurrentdir), nil, nil, SW_SHOW) end;

procedure TF1.Youtube1Click(Sender: TObject);
begin Shellexecute(Handle, 'open', PChar('http://www.youtube.com/results?search_query='+web_str(s[sel.show].name)+'&page=&utm_source=opensearch'), nil, nil, SW_SHOW) end;

procedure TF1.Youtubethemesong1Click(Sender: TObject);
begin Shellexecute(Handle, 'open', PChar('http://www.youtube.com/results?search_query='+web_str(s[sel.show].name)+'+theme+song&page=&utm_source=opensearch'), nil, nil, SW_SHOW) end;

procedure TF1.Photosfolder1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('data\photos'), nil, nil, SW_SHOW) end;

procedure TF1.Musicfolder1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('data\music'), nil, nil, SW_SHOW) end;

procedure TF1.Playonshowclick1Click(Sender: TObject);
begin Playonshowclick1.Checked:=not Playonshowclick1.Checked; sett.playonshowrefresh:=Playonshowclick1.Checked; save_settings end;

procedure TF1.Playonstartup1Click(Sender: TObject);
begin Playonstartup1.Checked:=not Playonstartup1.Checked; sett.playonstartup:=Playonstartup1.Checked; save_settings end;

procedure TF1.play_bClick(Sender: TObject);
begin
  try
  mpl.Close; mpl.FileName:='data\music\'+s[sel.show].name+'.mp3'; mpl.Open; mpl.Play; volTBChange(play_b);
  volTB.Visible:=true; play_b.Enabled:=false; Timer2.Enabled:=true;
  log('Playing "'+s[sel.show].name+'" theme (~'+inttostr(trunc(mpl.Length/1000) div 60)+':'+formatfloat('00', trunc(mpl.Length/1000) mod 60)+')...')
  //even if the song length is incorrect, there's not much i can (/know how to) do
  except on E:Exception do showmessage('An error has occured while trying to play the theme song.'+dnl+E.ClassName+' raised an error with the message "'+E.Message+'"') end
end;

procedure TF1.play_opt_bClick(Sender: TObject);
var p: TPoint;
begin getcursorpos(p); PopupMenu3.Popup(p.X-300, p.Y) end;

procedure TF1.PopupMenu1Popup(Sender: TObject);
begin EpisodeX1.Caption:='Episode '+s[sel.show].last_episode_title; Edit2.Caption:='Edit "'+s[sel.show].name+'"'; end;

procedure TF1.PopupMenu3Popup(Sender: TObject);
begin
  Playonshowclick2.Checked:=Playonshowclick1.Checked; Playonstartup2.Checked:=Playonstartup1.Checked;
  Stopplaying2.Enabled:=mpl.Mode=mpPlaying; Selectathemesong1.Caption:='Select a theme song for "'+s[sel.show].name+'"'
end;

procedure TF1.PopupMenu4Popup(Sender: TObject);
var b: boolean;
begin
  b:=fileexists('data\photos\'+s[sel.show].name+'.jpg');
  Openfullsizedimage1.Enabled:=b; Optionsforthisimage1.Visible:=true
end;

procedure TF1.ref_info_bClick(Sender: TObject);
var i: word; xs: string; xd: TDate;
begin
  log('Refreshing info for show "'+s[sel.show].name+'"...');
  if sel.show=0 then begin showmessage('Error... sel.show=0 ? (weird)'+dnl+'Click a show and try again.'); exit end;
  Label5.Caption:=stringreplace(s[sel.show].name, '&', '&&', [rfReplaceAll]);
  Label6.Caption:=d[sel.day].name+'s @ '+inttostr(s[sel.show].broadcast_hour)+':00';
  xd:=s[sel.show].last_episode_modified;
  if xd<encodedate(1901,1,1) then Label12.Caption:='' else Label12.Caption:='last modified '+formatdatetime('dddd, mmmm d', xd)+number_suffix(strtoint(formatdatetime('d', xd)))+formatdatetime(', yyyy "at" h:nn', xd);
  //
  xd:=s[sel.show].last_episode_date;
  if s[sel.show].last_episode=0 then Label7.Caption:='No info on last watched episode.'
  else if xd<encodedate(1901,1,1) then Label7.Caption:='Episode '+FormatLastEpisode(s[sel.show].last_episode)
  else Label7.Caption:='Episode '+FormatLastEpisode(s[sel.show].last_episode)+' on '+formatdatetime('dddd, mmmm d', xd)+number_suffix(strtoint(formatdatetime('d', xd)))+formatdatetime(', yyyy', xd);
  F1.info_labels_size_check(F1);
  //
  if fileexists('data\photos\'+s[sel.show].name+'.jpg') then imag.Picture.LoadFromFile('data\photos\'+s[sel.show].name+'.jpg') else imag.Picture.LoadFromFile('data\logo2.png');
  for i:=1 to 10 do if i<=s[sel.show].rating then img[i].Picture.LoadFromFile('data\star.png') else img[i].Picture.LoadFromFile('data\starG.png');
  xs:='data\music\'+s[sel.show].name+'.mp3';
  if mpl.FileName<>xs then
    begin
      play_b.Enabled:=fileexists(xs);
      if (Sender=Button1) and (not sett.playonstartup) then exit;
      if play_b.Enabled and sett.playonshowrefresh then play_bClick(ref_info_b)
    end
end;

procedure TF1.Selectthemesongforshow1Click(Sender: TObject);
var op: TOpenDialog; p, q: string; ok: boolean;
begin
  if sel.show=0 then begin showmessage('sel.show=0 ??? WTF? Exiting...'); exit end;
  op:=TOpenDialog.Create(Self); op.Title:='Select MP3 file as theme song for "'+s[sel.show].name+'"';
  op.Filter:='*.mp3'; op.Options:=[ofFileMustExist]; op.InitialDir:=GetDesktopFolder;
  if op.Execute then
    begin
      p:=op.Filename; q:='data\music\'+s[sel.show].name+'.mp3';
      if pos('HTTP://', ansiuppercase(p))<>0 then ok:=download(p, q)
      else ok:=copyfile(pwidechar(p), pwidechar(q), false);
      if ok then begin showmessage('Success!'); ref_info_bClick(Selectthemesongforshow1) end else showmessage('Copying the file has failed. Try again or do it manually.')
    end;
  op.Free
end;

procedure TF1.Shortcutkeys1Click(Sender: TObject);
begin showmessage('The following is a list of shortcut keys that can be called from anywhere within the application for ease of use:'+dnl+'Ctrl+A - toggle show all shows'+nl+'Ctrl+B - create data backup'+nl+'Ctrl+E - edit data'+nl+'Ctrl+F - filter shows'+nl+'Ctrl+H - version history'+nl+'Ctrl+L - open log file'+nl+'Ctrl+O - open program folder'+nl+'Ctrl+M - manage aux data'+nl+'Ctrl+S - stop playing music'+nl+'Ctrl+T - themes'+nl+'Ctrl+X - exit'+nl+'Ctrl+W - toggle see week Monday-Sunday'+dnl+'F1 - help: shortcut keys'+nl+'F5 - refresh shows'+nl+'F6 - random show'+nl+'F10 - see stats'+nl+'F11 - toggle fullscreen'+dnl+'Ctrl+F1 - sort shows by name'+nl+'Ctrl+F2 - sort shows by last episode'+nl+'Ctrl+F3 - sort shows by episode date'+nl+'Ctrl+F4 - sort shows by broadcasting hour'+nl+'Ctrl+F5 - sort shows by rating') end;

procedure TF1.Showallshows1Click(Sender: TObject);
begin
  Showallshows1.Checked:=not Showallshows1.Checked;
  refresh_bClick(Showallshows1)
end;

procedure TF1.Showdatafile1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('data\shows.xml'), nil, nil, SW_SHOW) end;

procedure TF1.Showhideapplication1Click(Sender: TObject);
begin
  if F1.Showing then begin F1.Hide; TrayIcon1.ShowBalloonHint end
  else F1.Show
end;

procedure TF1.Showinfoforrandomshow1Click(Sender: TObject);
var i: byte;
begin
  sel.show:=random(ns)+1; sel.day:=s[sel.show].dow; ref_info_bClick(FEd);
  for i:=1 to 7 do if lb[i].Items.IndexOf(lb_show_format(sel.show))<>-1 then lb[i].ItemIndex:=lb[i].Items.IndexOf(lb_show_format(sel.show)) else lb[i].ItemIndex:=-1
end;

procedure TF1.lb_click(Sender: TObject);
var x: string; i: byte;
begin
  if not (TListBox(Sender).ItemIndex in [0..TListBox(Sender).Items.Count-1]) then exit;
  if Sender<>FEd then begin sel.show:=0; sel.day:=0 end;
  x:=TListBox(Sender).Items[TListBox(Sender).ItemIndex];
  for i:=1 to ns do if x=lb_show_format(i) then
    begin sel.show:=i; sel.day:=s[i].dow; break end;
  if sel.show=0 then begin showmessage('Could not find the show "'+x+'", WTF?!'); exit end; ref_info_bClick(Sender);
  for i:=1 to 7 do if lb[i]<>Sender then lb[i].ItemIndex:=-1
end;

procedure TF1.lb_dbl_click(Sender: TObject);
begin
  if FS.Showing then begin FS.lb.ItemIndex:=FS.lb.Items.IndexOf(s[sel.show].name); FS.lbClick(Sender) end;
  if FEd.Showing then FEd.OnShow(Sender) else FEd.Show
end;

procedure TF1.ListBox1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var b: boolean;
begin
  if Button=mbRight then
    with TListBox(Sender) do
      begin
        if y div -Font.Height >= Items.Count then exit;
        ItemIndex:=y div -Font.Height; OnClick(Sender);
        lb_context_popup(TListBox(Sender), Point(X, Y), b)
      end;
end;

procedure TF1.Managephotosandmusic1Click(Sender: TObject);
begin if FM.Showing then FM.OnShow(Managephotosandmusic1) else FM.Show end;

procedure TF1.lb_context_popup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
  PopupMenu1.Popup(F1.Left+TListBox(Sender).Left+MousePos.X+12, F1.Top+TListBox(Sender).Top+MousePos.Y+30);
  Handled:=true
end;

initialization

//RegisterClass(TPngObject);
//TPicture.RegisterFileFormat('PNG', 'Portable Network Graphics', TPngObject);

end.
