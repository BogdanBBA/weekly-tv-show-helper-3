unit thE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, data_types;

type
  TThEd = class(TForm)
    Panel1: TPanel;
    titL: TLabel;
    s1L: TLabel;
    s2L: TLabel;
    tiL: TLabel;
    tL: TLabel;
    dowL: TLabel;
    dowlb: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    ColorBox1: TColorBox;
    clb1: TColorBox;
    Label17: TLabel;
    clb2: TColorBox;
    Label18: TLabel;
    Label19: TLabel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    edit_font_b: TButton;
    FontDialog1: TFontDialog;
    fd: TFontDialog;
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure clb1Change(Sender: TObject);
    procedure clb2Change(Sender: TObject);
    procedure edit_font_bClick(Sender: TObject);
    procedure titLClick(Sender: TObject);
    procedure s1LClick(Sender: TObject);
    procedure s2LClick(Sender: TObject);
    procedure tiLClick(Sender: TObject);
    procedure tLClick(Sender: TObject);
    procedure dowLClick(Sender: TObject);
    procedure dowlbClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ThEd: TThEd;
  tth: TTheme;
  tthN: string;

implementation

{$R *.dfm}

uses functii, ThU;

procedure TThEd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ThForm.thlbClick(ThEd)
end;

procedure TThEd.Button1Click(Sender: TObject);
begin
  ThEd.Close
end;

procedure TThEd.Button2Click(Sender: TObject);
begin
  try
  log('Saving theme "'+tthN+'" (in memory)...');
  tth.colors.form:=Panel1.Color; tth.colors.lbs:=dowlb.Color;
  tth.fonts.title:=fonttostr(titL.Font);
  tth.fonts.subtitle_date:=fonttostr(s1L.Font);
  tth.fonts.subtitle_ep:=fonttostr(s2L.Font);
  tth.fonts.today_is:=fonttostr(tiL.Font);
  tth.fonts.today_label:=fonttostr(tL.Font);
  tth.fonts.ldays:=fonttostr(dowL.Font);
  tth.fonts.lbs:=fonttostr(dowlb.Font);
  //
  save_theme(tthN, tth);
  log('Success!'); Button2.Caption:='Saved!'; Button2.Enabled:=false; Timer1.Enabled:=true;
  except on E:Exception do showmessage('An error has occured while saving theme.'+dnl+E.ClassName+': '+E.Message) end
end;

procedure TThEd.clb1Change(Sender: TObject);
begin Panel1.Color:=clb1.Selected end;

procedure TThEd.clb2Change(Sender: TObject);
begin dowlb.Color:=clb2.Selected end;

procedure TThEd.dowlbClick(Sender: TObject);
begin edit_font_bClick(dowlb) end;

procedure TThEd.dowLClick(Sender: TObject);
begin edit_font_bClick(dowL) end;

procedure TThEd.s1LClick(Sender: TObject);
begin edit_font_bClick(s1L) end;

procedure TThEd.s2LClick(Sender: TObject);
begin edit_font_bClick(s2L) end;

procedure TThEd.tiLClick(Sender: TObject);
begin edit_font_bClick(tiL) end;

procedure TThEd.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=false;
  Button1Click(Timer1)
end;

procedure TThEd.titLClick(Sender: TObject);
begin edit_font_bClick(titL) end;

procedure TThEd.tLClick(Sender: TObject);
begin edit_font_bClick(tL) end;

procedure TThEd.edit_font_bClick(Sender: TObject);
begin
  if Sender is TLabel then fd.Font:=TLabel(Sender).Font else fd.Font:=TListBox(Sender).Font;
  if fd.Execute then
    begin
      if Sender is TLabel then TLabel(Sender).Font:=fd.Font
      else TListBox(Sender).Font:=fd.Font
    end;
end;

procedure TThEd.FormShow(Sender: TObject);
var x: TFont;
begin
  try
  log('Initializing theme editor...');
  Button2.Caption:='Save'; Button2.Enabled:=true;
  tthN:=theme1_name; Label11.Caption:='Theme "'+tthN+'"';
  load_theme(tthN, tth);
  clb1.Selected:=tth.colors.form; clb2.Selected:=tth.colors.lbs; clb1Change(ThEd); clb2Change(ThEd); log('Applying fonts...');
  strtofont(tth.fonts.title, x); titL.Font:=x;
  strtofont(tth.fonts.subtitle_date, x); s1L.Font:=x;
  strtofont(tth.fonts.subtitle_ep, x); s2L.Font:=x;
  strtofont(tth.fonts.today_is, x); tiL.Font:=x;
  strtofont(tth.fonts.today_label, x); tL.Font:=x;
  strtofont(tth.fonts.ldays, x); dowl.Font:=x;
  strtofont(tth.fonts.lbs, x); dowlb.Font:=x;
  log('Success!')
  except on E:Exception do showmessage('An error has occured while initializign theme editor form.'+dnl+E.ClassName+': '+E.Message) end
end;

end.
